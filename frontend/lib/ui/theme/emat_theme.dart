import 'package:flutter/material.dart';

class EmatTheme {
  EmatTheme._();

  static const Color white = Colors.white;
  static const Color black = Colors.black;
  static const Color accent = Colors.green;

  static const Color statusBarBackgroundLight = Color(0xFFFAFAFA);
  static const Color statusBarBackgroundDark = Colors.black; //Color(0xFF303030);

  static ThemeData get lightTheme => ThemeData(
        appBarTheme: AppBarTheme(
          color: statusBarBackgroundLight,
          iconTheme: IconThemeData(
            color: black,
          ),
        ),
        iconTheme: IconThemeData(
          color: black,
        ),
        primaryColor: white,
        primaryColorBrightness: Brightness.light,
        accentColor: black,
        buttonTheme: ButtonThemeData(
          buttonColor: black,
          textTheme: ButtonTextTheme.primary,
        ),
        floatingActionButtonTheme: FloatingActionButtonThemeData(
          backgroundColor: black,
          foregroundColor: white,
        ),
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      );

  static ThemeData get darkTheme => ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.grey[850],
          iconTheme: IconThemeData(
            color: white,
          ),
        ),
        iconTheme: IconThemeData(
          color: white,
        ),
        scaffoldBackgroundColor: black,
        primaryColor: black,
        accentColor: accent,
        buttonTheme: ButtonThemeData(
          buttonColor: accent,
          textTheme: ButtonTextTheme.primary,
        ),
        floatingActionButtonTheme: FloatingActionButtonThemeData(
          backgroundColor: accent,
          foregroundColor: white,
        ),
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
        dividerColor: Colors.grey,
      );
}
