import 'package:emat/model/item.dart';
import 'package:emat/ui/theme/emat_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

enum ItemTableMode {
  take,
  store,
}

extension _ItemTableModeExtension on ItemTableMode {
  IconData getIcon() {
    if (this == ItemTableMode.take) {
      return Icons.unarchive;
    } else {
      return Icons.archive;
    }
  }

  String getName() {
    if (this == ItemTableMode.take) {
      return 'Take';
    } else {
      return 'Store';
    }
  }

  DismissDirection getDirection() {
    if (this == ItemTableMode.take) {
      return DismissDirection.startToEnd;
    } else {
      return DismissDirection.endToStart;
    }
  }

  Color getColor() {
    if (this == ItemTableMode.take) {
      return Colors.orangeAccent;
    } else {
      return Colors.green;
    }
  }

  Alignment getAlignment() {
    if (this == ItemTableMode.take) {
      return Alignment.centerLeft;
    } else {
      return Alignment.centerRight;
    }
  }
}

class ItemDataTable extends StatelessWidget {
  final List<Item> items;

  final Function onItemChanged;
  final ItemTableMode mode;

  ItemDataTable(this.items,
      {this.onItemChanged, this.mode = ItemTableMode.take});

  @override
  Widget build(BuildContext context) {
    void edit(Item item) async {
      await Navigator.pushNamed(context, '/item/edit', arguments: item);
      if (onItemChanged != null) {
        onItemChanged();
      }
    }

    Widget text(String text) {
      return ConstrainedBox(
        constraints: BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width * 0.5,
        ),
        child: Text(
          text,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(fontSize: 16),
          maxLines: 1,
          textWidthBasis: TextWidthBasis.parent,
        ),
      );
    }

    Container tableRow(Widget left, Widget right) {
      return Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 0),
        alignment: Alignment.centerLeft,
        height: 70,
        decoration: BoxDecoration(
          border: Border(bottom: Divider.createBorderSide(context, width: 0)),
          color: Theme.of(context).primaryColor,
        ),
        child: Row(
          children: [
            Expanded(flex: 3, child: left),
            Flexible(flex: 1, child: right),
          ],
        ),
      );
    }

    if (this.items.isEmpty) {
      return Column(
        children: [
          Divider(),
          ListTile(title: Text('No Items', textAlign: TextAlign.center)),
          Divider(),
        ],
      );
    }

    final list = ListView.builder(
      itemCount: this.items.length + 1,
      itemBuilder: (context, index) => index == this.items.length
          ? Container(
              color: Theme.of(context).primaryColor,
              padding: EdgeInsets.symmetric(vertical: 10),
              width: double.infinity,
              child: Text(
                'Total Items: ${this.items.length}',
                style: TextStyle(fontSize: 15),
                textAlign: TextAlign.center,
              ),
            )
          : Dismissible(
              key: UniqueKey(),
              direction: this.mode.getDirection(),
              onDismissed: (_) => this.onItemChanged(),
              background: Container(
                padding: EdgeInsets.only(left: 25, right: 25),
                alignment: this.mode.getAlignment(),
                color: this.mode.getColor(),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  textDirection: this.mode.getAlignment().x < 0
                      ? TextDirection.ltr
                      : TextDirection.rtl,
                  children: [
                    Icon(this.mode.getIcon(), color: EmatTheme.white, size: 30),
                    SizedBox(width: 10),
                    Text(
                      this.mode.getName(),
                      style:
                          const TextStyle(color: EmatTheme.white, fontSize: 20),
                    ),
                  ],
                ),
              ),
              child: InkWell(
                onTap: () => edit(this.items[index]),
                child: tableRow(
                  text(this.items[index].name),
                  text(this.items[index].amount == null
                      ? '-'
                      : this.items[index].amount.toString()),
                ),
              ),
            ),
    );

    final headerFont = TextStyle(
      fontSize: 15,
      fontWeight: FontWeight.w500,
      color: Theme.of(context).textTheme.bodyText1.color.withOpacity(0.5),
    );

    return Column(
      children: [
        Container(
          width: double.infinity,
          decoration: BoxDecoration(
            border: Border(bottom: Divider.createBorderSide(context, width: 0)),
          ),
        ),
        Container(
          height: 50,
          child: tableRow(
            Text('Name', style: headerFont),
            Text('Amount', style: headerFont),
          ),
        ),
        Expanded(child: list),
      ],
    );
  }
}
