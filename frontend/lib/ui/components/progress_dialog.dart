import 'package:emat/ui/theme/emat_theme.dart';
import 'package:flutter/material.dart';

class ProgressDialog {
  Dialog _progressDialog;
  BuildContext _context;
  bool _isShowing = false;

  ProgressDialog(BuildContext context, String message) {
    this._context = context;
    this._progressDialog = new Dialog(
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Row(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(10, 10, 20, 10),
              child: CircularProgressIndicator(
                backgroundColor: EmatTheme.white,
                valueColor: AlwaysStoppedAnimation<Color>(EmatTheme.accent),
              ),
            ),
            Text(message, style: Theme.of(context).textTheme.headline6),
          ],
        ),
      ),
    );
  }

  Future<void> show(bool isDismissible) async {
    showDialog(
      context: this._context,
      barrierDismissible: isDismissible ?? true,
      builder: (context) => this._progressDialog,
    );
    this._isShowing = true;
  }

  Future<void> hide() async {
    if (this._isShowing) {
      Navigator.pop(this._context);
      this._isShowing = false;
    }
  }
}
