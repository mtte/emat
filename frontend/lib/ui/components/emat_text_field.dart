import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// This file is ugly

class EmatNumberTextFormField extends EmatTextFormField {
  EmatNumberTextFormField({
    String labelText,
    bool enabled,
    TextEditingController controller,
    int maxLength,
    TextInputAction textInputAction,
    ValueChanged<String> onFieldSubmitted,
  }) : super(
          keyboardType: TextInputType.number,
          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          textInputAction: textInputAction,
          maxLength: maxLength,
          enabled: enabled,
          controller: controller,
          onFieldSubmitted: onFieldSubmitted,
          decoration: InputDecoration(
              labelText: labelText,
              border: const OutlineInputBorder(),
              filled: true,
              counterText: '',
              prefixIcon: IconButton(
                icon: Icon(Icons.remove),
                onPressed: () {
                  final value = int.tryParse(controller.text.trim());
                  if (value != null) {
                    final newValue = value - 1;
                    if (newValue >= 0 &&
                        newValue.toString().length <= maxLength) {
                      controller.text = newValue.toString();
                    }
                  }
                },
              ),
              suffixIcon: IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  final value = int.tryParse(controller.text.trim());
                  if (value != null) {
                    final newValue = value + 1;
                    if (newValue >= 0 &&
                        newValue.toString().length <= maxLength) {
                      controller.text = newValue.toString();
                    }
                  }
                },
              )),
        );
}

class EmatTextFormField extends TextFormField {
  EmatTextFormField({
    String labelText,
    Key key,
    TextEditingController controller,
    String initialValue,
    FocusNode focusNode,
    InputDecoration decoration,
    TextInputType keyboardType,
    TextCapitalization textCapitalization = TextCapitalization.none,
    TextInputAction textInputAction,
    TextStyle style,
    StrutStyle strutStyle,
    TextDirection textDirection,
    TextAlign textAlign = TextAlign.start,
    TextAlignVertical textAlignVertical,
    bool autofocus = false,
    bool readOnly = false,
    ToolbarOptions toolbarOptions,
    bool showCursor,
    String obscuringCharacter = '•',
    bool obscureText = false,
    bool autocorrect = true,
    SmartDashesType smartDashesType,
    SmartQuotesType smartQuotesType,
    bool enableSuggestions = true,
    bool autovalidate = false,
    bool maxLengthEnforced = true,
    int maxLines = 1,
    int minLines,
    bool expands = false,
    int maxLength,
    ValueChanged<String> onChanged,
    GestureTapCallback onTap,
    VoidCallback onEditingComplete,
    ValueChanged<String> onFieldSubmitted,
    FormFieldSetter<String> onSaved,
    FormFieldValidator<String> validator,
    List<TextInputFormatter> inputFormatters,
    bool enabled,
    double cursorWidth = 2.0,
    double cursorHeight,
    Radius cursorRadius,
    Color cursorColor,
    Brightness keyboardAppearance,
    EdgeInsets scrollPadding = const EdgeInsets.all(20.0),
    bool enableInteractiveSelection = true,
    InputCounterWidgetBuilder buildCounter,
    ScrollPhysics scrollPhysics,
    Iterable<String> autofillHints,
    AutovalidateMode autovalidateMode,
  })  : assert(decoration != null || labelText != null),
        super(
            key: key,
            controller: controller,
            initialValue: initialValue,
            focusNode: focusNode,
            keyboardType: keyboardType,
            textCapitalization: textCapitalization,
            textInputAction: textInputAction,
            style: style,
            strutStyle: strutStyle,
            textDirection: textDirection,
            textAlign: textAlign,
            textAlignVertical: textAlignVertical,
            autofocus: autofocus,
            readOnly: readOnly,
            toolbarOptions: toolbarOptions,
            showCursor: showCursor,
            obscuringCharacter: obscuringCharacter,
            obscureText: obscureText,
            autocorrect: autocorrect,
            smartDashesType: smartDashesType,
            smartQuotesType: smartQuotesType,
            enableSuggestions: enableSuggestions,
            autovalidate: autovalidate,
            maxLengthEnforced: maxLengthEnforced,
            maxLines: maxLines,
            minLines: minLines,
            expands: expands,
            maxLength: maxLength,
            onChanged: onChanged,
            onTap: onTap,
            onEditingComplete: onEditingComplete,
            onFieldSubmitted: onFieldSubmitted,
            onSaved: onSaved,
            validator: validator,
            inputFormatters: inputFormatters,
            enabled: enabled,
            cursorWidth: cursorWidth,
            cursorHeight: cursorHeight,
            cursorRadius: cursorRadius,
            cursorColor: cursorColor,
            keyboardAppearance: keyboardAppearance,
            scrollPadding: scrollPadding,
            enableInteractiveSelection: enableInteractiveSelection,
            buildCounter: buildCounter,
            scrollPhysics: scrollPhysics,
            autofillHints: autofillHints,
            autovalidateMode: autovalidateMode,
            decoration: decoration ??
                InputDecoration(
                  border: const OutlineInputBorder(),
                  filled: true,
                  labelText: labelText,
                  counterText: '',
                ));
}
