import 'package:emat/service/auth_service.dart' as AuthService;
import 'package:emat/ui/theme/emat_theme.dart';
import 'package:emat/util/helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../app_state_notifier.dart';
import '../../main.dart';

class EmatDrawer extends StatelessWidget {
  final EmatDrawerSelection selection;

  EmatDrawer({this.selection});

  @override
  Widget build(BuildContext context) {
    bool darkTheme = isUsingDarkTheme(context);

    Widget _createDrawerItem(
        {IconData icon,
        String text,
        GestureTapCallback onTap,
        EmatDrawerSelection selection}) {

      final selected = this.selection == selection;

      return ListTile(
        selected: selected,
        selectedTileColor: darkTheme ? Color(0x25FFFFFF) : Color(0x18000000),
        title: Text(
          text,
          style:
              TextStyle(color: darkTheme ? EmatTheme.white : EmatTheme.black),
        ),
        leading: Icon(
          icon,
          color: darkTheme ? EmatTheme.white : EmatTheme.black,
        ),
        onTap: () {
          if (selected) {
            Navigator.pop(context);
          } else {
            onTap();
          }
        },
      );
    }

    List<Widget> getMainMenuItems() {
      List<Widget> items = [];

      // Home
      items.add(
        _createDrawerItem(
          text: 'Home',
          icon: Icons.home_sharp,
          onTap: () => Navigator.pushNamedAndRemoveUntil(
              context, '/home', ModalRoute.withName('/')),
          selection: EmatDrawerSelection.home,
        ),
      );

      // Positions
      if (AuthService.isAdmin(EmatAppState.loggedInUser)) {
        items.add(_createDrawerItem(
          icon: Icons.room,
          text: 'Positions',
          onTap: () => Navigator.popAndPushNamed(context, '/positions'),
          selection: EmatDrawerSelection.positions,
        ));
      }

      // Users
      if (AuthService.isSuperAdmin(EmatAppState.loggedInUser)) {
        items.add(_createDrawerItem(
          icon: Icons.people_alt,
          text: 'Users',
          onTap: () => Navigator.popAndPushNamed(context, '/users'),
          selection: EmatDrawerSelection.users,
        ));
      }

      return items;
    }

    return Drawer(
      child: Container(
        color: darkTheme ? EmatTheme.black : EmatTheme.statusBarBackgroundLight,
        child: Column(
          children: [
            Expanded(
              child: ListView(
                children: getMainMenuItems(),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Column(
                children: [
                  ListTile(
                    title: Text(
                      'Dark Mode',
                    ),
                    leading: Icon(
                      Icons.brightness_6,
                      color: darkTheme ? EmatTheme.white : EmatTheme.black,
                    ),
                    trailing: Switch(
                      value: isUsingDarkTheme(context),
                      onChanged: (bool) {
                        Provider.of<AppStateNotifier>(context, listen: false)
                            .updateTheme(bool);
                      },
                      activeColor: Colors.lightGreenAccent,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 8, 0, 16),
                    child: FutureBuilder<String>(
                      future: getAppVersionInfo(),
                      builder: (context, snapshot) {
                        var text = 'Emat';
                        if (snapshot.hasData) {
                          text = snapshot.data;
                        }
                        return Text(
                          text,
                          style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.bold,
                          ),
                        );
                      },
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

}

enum EmatDrawerSelection {
  home,
  positions,
  users,
}