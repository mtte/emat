import 'dart:math';

import 'package:emat/main.dart';
import 'package:emat/service/auth_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoggedInUserInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Color _iconColor = Theme.of(context).iconTheme.color;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 8),
        ListTile(
          leading: CircleAvatar(
              child:
                  Text(EmatAppState.loggedInUser.username[0].toUpperCase())),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Logged in as',
                  style: Theme.of(context).textTheme.caption),
              Text(EmatAppState.loggedInUser.username),
            ],
          ),
        ),
        Divider(),
        ListTile(
          leading: Icon(Icons.settings, color: _iconColor),
          title: Text('Settings'),
        ),
        ListTile(
          leading: Transform.rotate(
            angle: pi,
            child: Icon(Icons.exit_to_app, color: _iconColor),
          ),
          title: Text('Logout'),
          onTap: () => logout(context),
        ),
      ],
    );
  }

}