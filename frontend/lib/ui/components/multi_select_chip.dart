import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MultiSelectChip<T> extends StatefulWidget {
  final Map<T, String> items;
  final Function(List<T>, bool, T) onSelectionChanged;
  final List<T> selectedChoices;
  final bool enabled;

  MultiSelectChip(
      {@required this.items,
      @required this.onSelectionChanged,
      this.selectedChoices = const [],
      this.enabled = true});

  @override
  _MultiSelectChipState createState() => _MultiSelectChipState<T>();

}

class _MultiSelectChipState<T> extends State<MultiSelectChip> {
  _buildChoiceList() {
    List<Widget> choices = List();

    widget.items.forEach((key, value) {
      choices.add(Container(
        padding: const EdgeInsets.all(2.0),
        child: ChoiceChip(
          label: Text(value),
          selected: widget.selectedChoices.contains(key),
          onSelected: (selected) {
            setState(() {
              if (!widget.enabled) {
                return;
              }

              bool added = !widget.selectedChoices.contains(key);
              added ? widget.selectedChoices.add(key)
                  : widget.selectedChoices.remove(key);
              widget.onSelectionChanged(widget.selectedChoices, added, key);
            });
          },
        ),
      ));
    });

    return choices;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: _buildChoiceList(),
    );
  }
}