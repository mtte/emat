import 'dart:convert';

import 'package:emat/model/user.dart';
import 'package:emat/service/auth_service.dart';
import 'package:emat/ui/components/multi_select_chip.dart';
import 'package:emat/ui/components/emat_text_field.dart';
import 'package:emat/ui/screens/view_edit_screen.dart';
import 'package:emat/util/api.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class UserScreen extends StatefulWidget {
  final User user;

  UserScreen({Key key, @required this.user}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _UserScreenState(this.user);
}

class _UserScreenState extends ViewEditScreenState<UserScreen> {
  User user;

  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  bool _showPasswordCleartext = false;

  _UserScreenState(this.user) : super('User', user.id == null);

  @override
  void initDefaultValues() {
    this._usernameController.text = this.user.username;
    this._passwordController.text = '';
  }

  @override
  void disposeControllers() {
    this._usernameController.dispose();
    this._passwordController.dispose();
  }

  @override
  void onEntitySuccessfullySaved(String savedUser) {
    this.user = User.fromJson(json.decode(savedUser));
  }

  @override
  Future<http.Response> deleteEntity() {
    return delete(context, '/users/${this.user.id}');
  }

  @override
  Future<http.Response> createEntity() {
    final data = {
      'username': _usernameController.text.trim(),
      'password': _passwordController.text.trim(),
      'roles': this.user.roles
    };
    return post(context, '/users', data);
  }

  @override
  Future<http.Response> updateEntity() {
    final data = <String, dynamic>{};
    if (_usernameController.text != this.user.username) {
      data['username'] = _usernameController.text;
    }

    if (_passwordController.text.isNotEmpty) {
      data['password'] = _passwordController.text;
    }

    if (data.isEmpty) {
      return null;
    }

    return put(context, '/users/${this.user.id}121212', data);
  }

  Map<String, String> _getRoles() {
    final roles = Map<String, String>();

    Role.values.forEach((r) {
      roles[r.name] = r.displayName;
    });

    return roles;
  }

  void addRole(String role) async {
    if (this.isNew) {
      setState(() {
        this.user.roles.add(role);
      });
      return;
    }

    final path = '/users/${this.user.id}/roles/$role';
    final result = await put(context, path, Map());

    if (result.statusCode == 200) {
      setState(() {
        this.user.roles.add(role);
      });
    }
  }

  void removeRole(String role) async {
    if (this.isNew) {
      setState(() {
        this.user.roles.remove(role);
      });
      return;
    }

    final path = '/users/${this.user.id}/roles/$role';
    final result = await delete(context, path);

    if (result.statusCode == 200) {
      setState(() {
        this.user.roles.remove(role);
      });
    }
  }

  String _validatePasswordField(String value) {
    String validation = validateInputField(value);
    if (this.isNew && validation != null) {
      return validation;
    }
    if (value.isNotEmpty && value.length < 8) {
      return 'Minimum length is 8';
    }
    return null;
  }

  @override
  List<Widget> buildFormWidgets() {
    final items = <Widget>[];

    // Id
    if (!this.isNew) {
      items.add(Text(
        'User ${this.user.id}',
        style: ViewEditScreenState.titleFont,
      ));
    }

    // Username
    items.add(EmatTextFormField(
      maxLength: 50,
      textInputAction: TextInputAction.next,
      validator: validateInputField,
      enabled: !this.readOnly,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      controller: _usernameController,
      labelText: 'Username',
    ));

    // Password
    items.add(EmatTextFormField(
      maxLength: 50,
      textInputAction: TextInputAction.done,
      validator: _validatePasswordField,
      enabled: !this.readOnly,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      controller: _passwordController,
      obscureText: !this._showPasswordCleartext,
      decoration: InputDecoration(
          suffixIcon: IconButton(
            icon: Icon(this._showPasswordCleartext
                ? Icons.visibility_off
                : Icons.visibility),
            onPressed: () => setState(() =>
            this._showPasswordCleartext = !this._showPasswordCleartext),
          ),
          border: const OutlineInputBorder(),
          filled: true,
          labelText: 'Password',
          hintText: this.isNew ? '' : 'Leave empty to keep old password'
      ),
    ));

    // Roles
    items.add(Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Roles',
          style: TextStyle(fontSize: 24),
        ),
        MultiSelectChip(
          items: _getRoles(),
          enabled: !this.readOnly,
          selectedChoices: this.user.roles,
          onSelectionChanged: (selection, added, role) =>
              added ? addRole(role) : removeRole(role),
        )
      ],
    ));

    return items;
  }

}