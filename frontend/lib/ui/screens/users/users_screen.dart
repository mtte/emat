import 'dart:async';
import 'dart:convert';

import 'package:emat/model/user.dart';
import 'package:emat/ui/components/emat_drawer.dart';
import 'package:emat/util/api.dart';
import 'package:flutter/material.dart';

class UsersScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _UsersScreenState();
}

class _UsersScreenState extends State<UsersScreen> {
  final _listItemFont = TextStyle(fontSize: 18);

  final _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  Future<List<User>> futureUsers;

  @override
  void initState() {
    super.initState();
    futureUsers = _fetchUsers();
  }

  Future<List<User>> _fetchUsers() async {
    final result = await get(context, '/users');
    if (result.statusCode != 200) {
      print(result.body);
      throw "Error fetching users";
    }

    final List users = json.decode(result.body);
    return users.map((u) => User.fromJson(u)).toList();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Users'),
      ),
      drawer: EmatDrawer(selection: EmatDrawerSelection.users),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _editUser(User.create()),
        tooltip: 'Add User',
        child: Icon(Icons.person_add),
      ),
      body: RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: () async => setState(() {
          this.futureUsers = _fetchUsers();
        }),
        child: FutureBuilder<List<User>>(
          future: futureUsers,
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return Center(child: Text(snapshot.error.toString()));
            }
            return ListView.builder(
              padding: EdgeInsets.zero,
              itemCount: snapshot.data == null ? 0 : snapshot.data.length,
              itemBuilder: (context, i) {
                return _buildUserRow(snapshot.data[i]);
              },
            );
          },
        ),
      )
    );
  }

  Widget _buildUserRow(User user) {
    return ListTile(
      contentPadding: EdgeInsets.fromLTRB(32, 10, 32, 10),
      title: Text(
        user.username,
        style: _listItemFont,
      ),
      leading: Text(
        user.id.toString(),
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
      ),
      onTap: () => _editUser(user),
    );
  }

  void _editUser(User user) async {
    await Navigator.pushNamed(context, '/users/edit', arguments: user);
    setState(() {
      this.futureUsers = _fetchUsers();
    });
  }
  
}
