import 'dart:convert';

import 'package:emat/model/inventory_list.dart';
import 'package:emat/service/auth_service.dart';
import 'package:emat/ui/components/logged_in_user_info.dart';
import 'package:emat/ui/components/emat_drawer.dart';
import 'package:emat/ui/screens/qr_scan_screen.dart';
import 'package:emat/ui/theme/emat_theme.dart';
import 'package:emat/util/api.dart';
import 'package:emat/util/constants.dart';
import 'package:floating_search_bar/floating_search_bar.dart';
import 'package:flutter/material.dart';

import 'package:emat/main.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  final _colors = <MaterialColor>[...Colors.primaries];
  final _searchTextController = TextEditingController();

  Future<List<InventoryList>> _futureData;

  bool _isSearching = false;

  @override
  void initState() {
    super.initState();

    // This one is too bright for white font
    _colors.remove(Colors.yellow);

    _futureData = _fetchData();
  }

  @override
  void dispose() {
    this._searchTextController.dispose();
    super.dispose();
  }

  Future<List<InventoryList>> _fetchData() async {
    final response = await get(context, '/inventories/$INVENTORY_ID/lists');
    if (response.statusCode != 200) {
      print(response.body);
      throw "Error fetching data";
    }

    final List lists = json.decode(response.body);
    return lists.map((l) => InventoryList.fromJson(l)).toList();
  }

  @override
  Widget build(BuildContext context) {
    Widget _buildGridItem(InventoryList list, int index) {
      return Container(
        decoration: BoxDecoration(
          color: _colors[index % _colors.length],
        ),
        child: ListTile(
          onTap: () => _list(list),
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                '${list.id}',
                style: TextStyle(
                  fontSize: 64.0,
                  fontWeight: FontWeight.w300,
                  color: EmatTheme.white,
                ),
              ),
              Text(
                list.name,
                style: TextStyle(fontSize: 16, color: EmatTheme.white),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      );
    }

    void _showUserDialog() {
      showDialog(
        context: context,
        child: Dialog(child: LoggedInUserInfo()),
        barrierDismissible: true,
      );
    }

    return Scaffold(
      drawer: EmatDrawer(selection: EmatDrawerSelection.home),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Visibility(
            visible: isAdmin(EmatAppState.loggedInUser),
            child: FloatingActionButton(
              heroTag: null,
              onPressed: () => _edit(InventoryList()),
              tooltip: 'Create List',
              child: Icon(Icons.add),
              mini: true,
            ),
          ),
          SizedBox(height: 8),
          FloatingActionButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => QRScanScreen()));
            },
            tooltip: 'Scan QR code',
            child: Icon(Icons.qr_code_scanner_sharp),
          ),
        ],
      ),
      body: NestedScrollView(
        headerSliverBuilder: (context, enabled) {
          return [
            SliverPadding(
              padding: EdgeInsets.only(top: 6.0, bottom: 16.0),
              sliver: SliverFloatingBar(
                elevation: 5.0,
                leading: IconButton(
                  padding: EdgeInsets.zero,
                  alignment: Alignment.centerLeft,
                  icon: Icon(
                    Icons.menu,
                    color: Theme.of(context).appBarTheme.iconTheme.color,
                  ),
                  onPressed: () => Scaffold.of(context).openDrawer(),
                ),
                title: TextField(
                  style: TextStyle(fontSize: 17),
                  controller: this._searchTextController,
                  textInputAction: TextInputAction.search,
                  textAlignVertical: TextAlignVertical.center,
                  onChanged: (_) => setState(() => this._isSearching =
                      this._searchTextController.text.isNotEmpty),
                  onSubmitted: (_) => _search(),
                  autofocus: false,
                  decoration: InputDecoration(
                    hintText: "Search in Emat...",
                    suffixIcon: Visibility(
                      visible: this._isSearching,
                      child: IconButton(
                        icon: Icon(
                          Icons.clear,
                          color: Theme.of(context).appBarTheme.iconTheme.color,
                        ),
                        onPressed: () => setState(() {
                          this._searchTextController.clear();
                          this._isSearching = false;
                        }),
                      ),
                    ),
                    border: InputBorder.none,
                  ),
                ),
                trailing: GestureDetector(
                  onTap: _showUserDialog,
                  child: CircleAvatar(
                    child: Text(
                      EmatAppState.loggedInUser.username[0].toUpperCase(),
                    ),
                  ),
                ),
              ),
            ),
          ];
        },
        body: RefreshIndicator(
          key: this._refreshIndicatorKey,
          onRefresh: () async => setState(() {
            this._futureData = _fetchData();
          }),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 16),
                child: Text(
                  'Hello ${EmatAppState.loggedInUser.username}!',
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                ),
              ),
              Expanded(
                child: FutureBuilder<List<InventoryList>>(
                  future: _futureData,
                  builder: (context, snapshot) {
                    if (snapshot.hasError) {
                      return Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text('Error getting data!'),
                            Text('Check your network connection.')
                          ],
                        ),
                      );
                    }
                    return GridView.builder(
                      padding: EdgeInsets.only(top: 12),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                      ),
                      itemCount:
                          snapshot.data == null ? 0 : snapshot.data.length,
                      itemBuilder: (context, i) =>
                          _buildGridItem(snapshot.data[i], i),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _list(InventoryList list) async {
    await Navigator.pushNamed(context, '/list', arguments: list);
    setState(() {
      this._futureData = _fetchData();
    });
  }

  void _edit(InventoryList list) async {
    await Navigator.pushNamed(context, '/list/edit', arguments: list);
    setState(() {
      this._futureData = _fetchData();
    });
  }

  void _search() async {
      // Do the search
      final query = this._searchTextController.text.trim();
      if (query.isNotEmpty) {
        await Navigator.pushNamed(context, '/search', arguments: query);
      }
      setState(() {
        this._searchTextController.clear();
        this._isSearching = false;
      });
    }
}
