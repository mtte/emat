import 'package:emat/ui/theme/emat_theme.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QRScanScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _QRScanScreenState();
}

class _QRScanScreenState extends State<QRScanScreen> {
  final key = GlobalKey();

  bool flashOn = false;
  QRViewController controller;

  void _onQrScan(String code) {
    this.controller.pauseCamera();

    print('Scanned QR code: $code');

    if (code.startsWith("EMAT:")) {
      if (code.startsWith("EMAT:LIST:")) {
        final id = code.substring("EMAT:LIST:".length);
        if (id.isNotEmpty) {
          // TODO: Fetch list and show screen
          return;
        }
      }
    }

    this.controller.resumeCamera();
  }

  @override
  void dispose() {
    this.controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    void _onQRViewCreated(QRViewController controller) {
      this.controller = controller;
      controller.scannedDataStream.listen(_onQrScan);
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Scan a QR Code'),
        centerTitle: true,
        actions: [
          IconButton(
              icon: Icon(this.flashOn ? Icons.flash_on : Icons.flash_off),
              onPressed: () {
                if (this.controller != null) {
                  this.controller.toggleFlash();
                  setState(() {
                    this.flashOn = !this.flashOn;
                  });
                }
              }),
        ],
      ),
      body: QRView(
        key: this.key,
        onQRViewCreated: _onQRViewCreated,
        overlay: QrScannerOverlayShape(
          borderColor: EmatTheme.accent,
          borderLength: 40,
          borderWidth: 10,
          cutOutSize: 250,
        ),
      ),
    );
  }
}
