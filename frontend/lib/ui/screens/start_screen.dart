
import 'dart:convert';

import 'package:emat/config/config.dart';
import 'package:emat/config/secure_config.dart';
import 'package:emat/main.dart';
import 'package:emat/model/user.dart';
import 'package:emat/util/api.dart';
import 'package:flutter/material.dart';

class StartScreen extends StatefulWidget {
  @override
  State createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  final _backgroundImage = 'assets/images/login_screen_background.jpg';

  Future checkIfUserAlreadyLoggedIn() async {
    if (EmatAppState.loggedInUser == null) {
      // Try to load user
      final userJson =
      await SecureConfig().getValue(ConfigKey.lastLoggedInUser);
      if (userJson == null) {
        Navigator.pushReplacementNamed(context, '/login');
      } else {
        EmatAppState.loggedInUser = LoggedInUser.fromJson(json.decode(userJson));
        get(context, '/authcheck');
        Navigator.pushReplacementNamed(context, '/home');
      }
    } else {
      get(context, '/authcheck');
      Navigator.pushReplacementNamed(context, '/home');
    }
  }

  @override
  void initState() {
    super.initState();

    checkIfUserAlreadyLoggedIn();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(this._backgroundImage),
                fit: BoxFit.fill,
                alignment: Alignment.topCenter
            )
        ),
      ),
    );
  }
}
