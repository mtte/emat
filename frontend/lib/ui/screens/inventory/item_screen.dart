import 'dart:convert';

import 'package:emat/model/item.dart';
import 'package:emat/ui/components/emat_text_field.dart';
import 'package:emat/ui/screens/view_edit_screen.dart';
import 'package:emat/ui/theme/emat_theme.dart';
import 'package:emat/util/api.dart';
import 'package:emat/util/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class ItemScreen extends StatefulWidget {
  final Item item;

  ItemScreen({Key key, @required this.item}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ItemScreenState(this.item);
}

class _ItemScreenState extends ViewEditScreenState<ItemScreen> {
  Item _item;

  final _nameController = TextEditingController();
  final _descriptionController = TextEditingController();
  final _amountController = TextEditingController();
  final _amountUnitController = TextEditingController();

  final DateFormat _dateFormatter = DateFormat('dd.MM.yyyy hh:mm');

  _ItemScreenState(this._item) : super('Item', _item.id == null);

  get itemsUrl {
    return '/inventories/$INVENTORY_ID/lists/${_item.inventoryListId}/items';
  }

  @override
  void initDefaultValues() {
    this._nameController.text = this._item.name;
    this._descriptionController.text = this._item.description;
    this._amountController.text =
    this._item.amount == null ? '1' : this._item.amount.toString();
    this._amountUnitController.text = this._item.amountUnit;
  }

  @override
  void disposeControllers() {
    this._nameController.dispose();
    this._descriptionController.dispose();
    this._amountController.dispose();
    this._amountUnitController.dispose();
  }

  @override
  void onEntitySuccessfullySaved(String savedItem) {
    this._item = Item.fromJson(json.decode(savedItem));
  }

  @override
  Future<http.Response> deleteEntity() {
    return delete(context, '$itemsUrl/${this._item.id}');
  }

  @override
  Future<http.Response> createEntity() {
    final data = {
      'name': this._nameController.text.trim(),
      'description': this._descriptionController.text.trim(),
      'amount': int.tryParse(this._amountController.text.trim()),
      'unit': this._amountUnitController.text.trim(),
    };

    final description = this._descriptionController.text.trim();
    if (description != null && description.isNotEmpty) {
      data['description'] = description;
    }

    return post(context, '$itemsUrl', data);
  }

  @override
  Future<http.Response> updateEntity() {
    final data = <String, dynamic>{};

    final name = this._nameController.text.trim();
    if (name != this._item.name) {
      data['name'] = name;
    }

    final description = this._descriptionController.text.trim();
    if (description != null &&
        description.isNotEmpty &&
        description != this._item.description) {
      data['description'] = description;
    }

    final amount = int.tryParse(this._amountController.text.trim());
    if (amount != null &&
        amount != this._item.amount) {
      data['amount'] = amount;
    }

    final unit = this._amountUnitController.text.trim();
    if (unit != null &&
        unit.isNotEmpty &&
        unit != this._item.amountUnit) {
      data['unit'] = unit;
    }

    if (data.isEmpty) {
      return null;
    }

    return put(context, '$itemsUrl/${this._item.id}', data);
  }

  @override
  List<Widget> buildFormWidgets() {
    final items = <Widget>[];

    // Id
    if (!this.isNew) {
      items.add(Text(
        this._nameController.text,
        style: ViewEditScreenState.titleFont,
      ));
    }

    // Details
    if (!this.isNew && this.readOnly) {
      items.add(Card(
        margin: EdgeInsets.zero,
        color: Theme.of(context).accentColor,
        child: Padding(
          padding: EdgeInsets.all(8),
          child: _buildDetailsWidget(),
        ),
      ));
    }

    // Name
    items.add(EmatTextFormField(
      maxLength: 255,
      textInputAction: TextInputAction.next,
      validator: validateInputField,
      enabled: !this.readOnly,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      controller: this._nameController,
      labelText: 'Name',
    ));

    // Description
    items.add(EmatTextFormField(
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      enabled: !this.readOnly,
      controller: this._descriptionController,
      labelText: 'Description',
    ));

    // Unit
    items.add(EmatTextFormField(
      maxLength: 25,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      enabled: !this.readOnly,
      controller: this._amountUnitController,
      labelText: 'Unit',
    ));

    // Amount
    items.add(EmatNumberTextFormField(
      textInputAction: TextInputAction.done,
      labelText: 'Amount',
      enabled: !this.readOnly,
      controller: this._amountController,
      maxLength: 5,
    ));

    return items;
  }

  Widget _buildDetailsWidget() {
    final detailsFont = TextStyle(color: EmatTheme.white);
    final boldFont = detailsFont.merge(TextStyle(fontWeight: FontWeight.bold));
    return ListTile(
      title: Column(
        children: [
          Row(
            children: [
              Text('Created by ', style: detailsFont),
              Text(_item.createdBy, style: boldFont),
              Text(' at ', style: detailsFont),
              Text(_dateFormatter.format(_item.created), style: boldFont,),
            ],
          ),
          SizedBox(height: 8),
          Row(
            children: [
              Text('Modified by ', style: detailsFont),
              Text(_item.modifiedBy, style: boldFont),
              Text(' at ', style: detailsFont),
              Text(_dateFormatter.format(_item.modified), style: boldFont,),
            ],
          ),
        ],
      ),
    );
  }
}
