import 'dart:convert';

import 'package:emat/main.dart';
import 'package:emat/model/inventory_list.dart';
import 'package:emat/model/item.dart';
import 'package:emat/service/auth_service.dart';
import 'package:emat/ui/components/item_data_table.dart';
import 'package:emat/ui/theme/emat_theme.dart';
import 'package:emat/util/api.dart';
import 'package:emat/util/helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class InventoryListScreen extends StatefulWidget {
  final InventoryList list;

  InventoryListScreen({Key key, @required this.list}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _InventoryListScreenState(this.list);
}

class _InventoryListScreenState extends State<InventoryListScreen> {
  InventoryList _list;

  Future<List<Item>> _futureItems;

  final _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  _InventoryListScreenState(this._list);

  @override
  void initState() {
    super.initState();
    this._futureItems = _fetchItems();
  }

  Future<List<Item>> _fetchItems() async {
    final response = await get(
        context, '/inventories/${_list.inventoryId}/lists/${_list.id}/items');

    if (response.statusCode != 200) {
      print(response.body);
      throw "Error fetching items";
    }

    final List items = json.decode(response.body);
    return items.map((i) => Item.fromJson(i)).toList();
  }

  @override
  Widget build(BuildContext context) {
    final darkTheme = isUsingDarkTheme(context);

    List<Widget> _getActions() {
      final actions = <Widget>[];

      if (isAdmin(EmatAppState.loggedInUser)) {
        actions.add(IconButton(
          icon: Icon(Icons.edit),
          onPressed: _editHeader,
        ));
      }

      return actions;
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title:
            Text('${this._list.name == null ? 'New List' : this._list.name}'),
        actions: _getActions(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => _addItem(),
      ),
      body: RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: () async => _refresh(),
        child: FutureBuilder<List<Item>>(
          future: _futureItems,
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return Center(child: Text(snapshot.error.toString()));
            }
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());
            }
            if (snapshot.data.isEmpty) {
              return _InventoryListHeader(
                this._list,
                child: Center(
                    child: Text(
                  'No Items Existing',
                  style: TextStyle(fontSize: 24),
                )),
              );
            }

            return DefaultTabController(
              length: 2,
              child: NestedScrollView(
                headerSliverBuilder: (context, innerBoxIsScrolled) => [
                  SliverToBoxAdapter(
                    child: _InventoryListHeader(
                      this._list,
                      child: Container(
                        padding: EdgeInsets.only(top: 7.5),
                        decoration: BoxDecoration(
                            color:
                                darkTheme ? EmatTheme.black : EmatTheme.white,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: darkTheme
                                    ? Color(0x00000000)
                                    : Color(0x10000000),
                                offset: Offset(0, -4),
                                blurRadius: 4,
                              spreadRadius: 1,
                              )
                            ]),
                        child: TabBar(
                          tabs: [
                            Tab(child: Text('Available')),
                            Tab(child: Text('Taken')),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
                body: Container(
                  child: TabBarView(
                    children: [
                      ItemDataTable(
                        snapshot.data,
                        mode: ItemTableMode.take,
                        onItemChanged: _refresh,
                      ),
                      ItemDataTable(
                        snapshot.data,
                        mode: ItemTableMode.store,
                        onItemChanged: _refresh,
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  void _refresh() {
    final items = _fetchItems();
    setState(() {
      _futureItems = items;
    });
  }

  void _editHeader() async {
    final result =
        await Navigator.pushNamed(context, '/list/edit', arguments: this._list);

    if (result == null) {
      Navigator.pop(context);
      return;
    }

    setState(() {
      this._list = result;
    });
  }

  void _addItem() async {
    await Navigator.pushNamed(context, '/item/edit',
        arguments: Item(inventoryListId: this._list.id));

    setState(() {
      this._futureItems = _fetchItems();
    });
  }
}

class _InventoryListHeader extends StatelessWidget {
  final _biggerFont = TextStyle(fontSize: 18, fontWeight: FontWeight.w600);

  final InventoryList list;
  final Widget child;

  _InventoryListHeader(this.list, {@required this.child});

  Widget _buildHeader(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(16, 24, 16, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Card(
            margin: EdgeInsets.zero,
            color: Theme.of(context).accentColor,
            child: ListTile(
              title: Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Text(
                      'ID: ${this.list.id}',
                      style: this._biggerFont.merge(
                            TextStyle(color: EmatTheme.white),
                          ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Text(
                      'Type: ${this.list.type}',
                      style: this._biggerFont.merge(
                            TextStyle(color: EmatTheme.white),
                          ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 30),
          Text(
            'Position',
            style: this._biggerFont,
          ),
          SizedBox(height: 6),
          Card(
            margin: EdgeInsets.zero,
            child: ListTile(
              leading: Icon(
                Icons.room,
                color: Theme.of(context).iconTheme.color,
              ),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(this.list.position.name),
                  Text(
                    this.list.position.description,
                    style: Theme.of(context).textTheme.caption,
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 35),
          Text(
            'Items',
            style: this._biggerFont,
          ),
          SizedBox(height: 12),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _buildHeader(context),
        this.child,
      ],
    );
  }
}
