import 'dart:convert';

import 'package:emat/model/inventory_list.dart';
import 'package:emat/model/position.dart';
import 'package:emat/ui/components/emat_text_field.dart';
import 'package:emat/ui/components/progress_dialog.dart';
import 'package:emat/ui/screens/view_edit_screen.dart';
import 'package:emat/ui/theme/emat_theme.dart';
import 'package:emat/util/api.dart';
import 'package:emat/util/constants.dart';
import 'package:emat/util/helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class EditInventoryListScreen extends StatefulWidget {
  final InventoryList list;

  EditInventoryListScreen({Key key, @required this.list}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _EditInventoryListScreenState(this.list);
}

class _EditInventoryListScreenState extends State<EditInventoryListScreen> {
  InventoryList _list;
  bool _isNew;

  final _formKey = GlobalKey<FormState>();

  final _nameController = TextEditingController();
  final _typeController = TextEditingController();

  Position _selectedPosition;
  Future<List<Position>> _futurePositions;

  _EditInventoryListScreenState(this._list) {
    this._isNew = this._list.id == null;
  }

  Future<List<Position>> _fetchPositions() async {
    final result = await get(context, '/positions');
    if (result.statusCode != 200) {
      print(result.body);
      throw "Error fetching positions";
    }

    final List positions = json.decode(result.body);
    return positions.map((p) => Position.fromJson(p)).toList();
  }

  void _save() async {
    if (!_formKey.currentState.validate()) {
      return;
    }

    try {
      http.Response response;
      if (this._isNew) {
        response = await _createInventoryList();
      } else {
        response = await _updateInventoryList();
      }

      if (response == null || response.statusCode == 200) {
        this._list.name = this._nameController.text.trim();
        this._list.type = this._typeController.text.trim();
        this._list.position = this._selectedPosition;
        Navigator.pop(context, this._list);
      } else if (response.statusCode == 400) {
        final error = json.decode(response.body);
        showAlertDialog(context, 'Saving Failed', error['message']);
      }
    } catch (e) {
      showAlertDialog(context, 'Ooops...\nSomething went wrong', e.toString());
    }
  }

  Future<http.Response> _createInventoryList() async {

    final data = {
      'name': this._nameController.text.trim(),
      'type': this._typeController.text.trim(),
      'positionId': this._selectedPosition.id,
    };

    return post(context,
        '/inventories/$INVENTORY_ID/lists', data);
  }

  Future<http.Response> _updateInventoryList() async {
    final data = <String, dynamic>{};

    final name = this._nameController.text.trim();
    if (name != this._list.name) {
      data['name'] = name;
    }

    final type = this._typeController.text.trim();
    if (type != this._list.type) {
      data['type'] = type;
    }

    if (this._selectedPosition != this._list.position) {
      data['positionId'] = this._selectedPosition.id;
    }

    if (data.isEmpty) {
      return null;
    }

    return put(context,
        '/inventories/${this._list.inventoryId}/lists/${this._list.id}', data);
  }

  void _delete() async {
    // TODO: Add confirmation dialog

    if (this._isNew) {
      return;
    }

    try {
      final response = await delete(context,
          '/inventories/${this._list.inventoryId}/lists/${this._list.id}');

      if (response.statusCode == 200) {
        Navigator.pop(context);
      }
    } catch (e) {
      showAlertDialog(context, "Ooops...\nSomething went wrong", e.toString());
    }
  }

  String _validateInputField(String value) {
    if (value.trim().isEmpty) {
      return 'Please fill out this field';
    }
    return null;
  }

  String _validatePositionDropdown(Position position) {
    if (position == null) {
      return 'Please fill out this field';
    }
    return null;
  }

  @override
  void initState() {
    super.initState();
    this._nameController.text = this._list.name;
    this._typeController.text = this._list.type;

    this._futurePositions = _fetchPositions();
  }

  @override
  void dispose() {
    this._nameController.dispose();
    this._typeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _getActions() {
      final actions = <Widget>[];

      // Save action
      actions.add(IconButton(
        icon: Icon(Icons.save),
        onPressed: () => _save(),
      ));

      // Delete actions
      if (!this._isNew) {
        actions.add(IconButton(
            icon: Icon(Icons.delete),
            onPressed: () => _delete()));
      }

      return actions;
    }

    List<Widget> _buildListForm() {
      final items = <Widget>[];

      // Id
      if (!this._isNew) {
        items.add(Text(
          'List ${this._list.id}',
          style: ViewEditScreenState.titleFont,
        ));
        items.add(SizedBox(height: 20));
      }

      // Name
      items.add(EmatTextFormField(
        maxLength: 255,
        textInputAction: TextInputAction.next,
        validator: _validateInputField,
        onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
        controller: this._nameController,
        labelText: 'Name',
      ));
      items.add(SizedBox(height: 30));

      // Type
      items.add(EmatTextFormField(
        maxLength: 255,
        textInputAction: TextInputAction.next,
        onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
        validator: _validateInputField,
        controller: this._typeController,
        labelText: 'Type',
      ));
      items.add(SizedBox(height: 30));

      // Position
      items.add(FutureBuilder<List<Position>>(
        future: _futurePositions,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text('Could not fetch positions'));
          }
          if (!snapshot.hasData) {
            return Center(child: Text('Loading...'));
          }
          if (snapshot.data.isEmpty) {
            return Center(child: Text('No positions available'));
          }

          final items = snapshot.data.map((position) {
            return DropdownMenuItem<Position>(
              value: position,
              child: Text('${position.name} (${position.description})'),
            );
          }).toList();

          if (this._list.position != null && this._selectedPosition == null) {
            for (final position in snapshot.data) {
              if (position.id == this._list.position.id) {
                this._selectedPosition = position;
              }
            }
          }

          return DropdownButtonFormField<Position>(
            value: this._selectedPosition,
            validator: _validatePositionDropdown,
            decoration: InputDecoration(
              border: const OutlineInputBorder(),
              filled: true,
              labelText: 'Position'
            ),
            isExpanded: true,
            items: items,
            onChanged: (value) => setState(() {
              this._selectedPosition = value;
            }),
          );
        },
      ));

      return items;
    }

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, this._list),
        ),
        centerTitle: true,
        title: Text(this._isNew ? 'Create List' : 'Edit List'),
        actions: _getActions(),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(16.0, 32.0, 16.0, 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _buildListForm(),
            ),
          ),
        ),
      ),
    );
  }
}
