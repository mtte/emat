import 'dart:convert';

import 'package:emat/config/config.dart';
import 'package:emat/config/secure_config.dart';
import 'package:emat/main.dart';
import 'package:emat/model/user.dart';
import 'package:emat/ui/components/progress_dialog.dart';
import 'package:emat/ui/theme/emat_theme.dart';
import 'package:emat/util/api.dart';
import 'package:emat/util/helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _backgroundImage = 'assets/images/login_screen_background.jpg';

  final _formKey = GlobalKey<FormState>();

  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  bool _previousUserPresent = false;
  bool _loggingIn = false;

  @override
  void initState() {
    super.initState();
    final previousUser = EmatAppState.loggedInUser;
    if (previousUser != null) {
      _previousUserPresent = true;
      _usernameController.text = previousUser.username;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      resizeToAvoidBottomInset: false,
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(this._backgroundImage),
            fit: BoxFit.fitWidth,
            alignment: Alignment.topCenter
          )
        ),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 65.0),
                  child: Text(
                    'EMAT',
                    style: TextStyle(
                      fontSize: 64.0,
                      fontWeight: FontWeight.w200,
                      color: EmatTheme.black
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: isUsingDarkTheme(context)
                      ? EmatTheme.black
                      : EmatTheme.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(20, 16, 20, 16),
                  child: Column(
                    children: [
                      TextFormField(
                        textInputAction: TextInputAction.next,
                        validator: _validateInputField,
                        onFieldSubmitted: (_) =>
                            FocusScope.of(context).nextFocus(),
                        controller: _usernameController,
                        decoration: InputDecoration(
                          labelText: 'Username',
                        ),
                      ),
                      TextFormField(
                        textInputAction: TextInputAction.done,
                        validator: _validateInputField,
                        onFieldSubmitted: (_) => submit(),
                        controller: _passwordController,
                        decoration: InputDecoration(
                          labelText: 'Password',
                        ),
                        obscureText: true,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20, bottom: 10),
                        child: Container(
                          width: double.infinity,
                          child: RaisedButton(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Visibility(
                                    visible: this._loggingIn,
                                    maintainState: true,
                                    maintainAnimation: true,
                                    maintainSize: true,
                                    child: Container(
                                        margin: EdgeInsets.only(
                                            right: 16, top: 2, bottom: 2),
                                        width: 20,
                                        height: 20,
                                        child: CircularProgressIndicator(
                                          strokeWidth: 3,
                                          backgroundColor: EmatTheme.white,
                                        ))),
                                Text(
                                  'Log In',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Container(
                                    margin: EdgeInsets.only(left: 16),
                                    width: 20,
                                    height: 0,
                                    child: CircularProgressIndicator()),
                              ],
                            ),
                            padding: EdgeInsets.only(top: 12, bottom: 12),
                            onPressed: () => submit(),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  submit() async {
    if (!_formKey.currentState.validate()) {
      return;
    }

    this._loggingIn = true;

    final data = {
      'username': _usernameController.text.trim(),
      'password': _passwordController.text.trim(),
    };
    try {
      final response = await post(context, '/login', data);

      if (response.statusCode == 200) {
        // Login success
        final user = LoggedInUser.fromJson(json.decode(response.body));

        EmatAppState.loggedInUser = user;

        await SecureConfig().setValue(ConfigKey.lastLoggedInUser, response.body);

        if (_previousUserPresent) {
          Navigator.pop(context);
        } else {
          Navigator.pushReplacementNamed(context, '/home');
        }
      } else if (response.statusCode == 401) {
        // Login failed
        showAlertDialog(context, 'Login Failed',
            'Incorrect username and/or password');
      } else if (response.statusCode == 400) {
        // Login request was invalid
        showAlertDialog(context, 'Login Failed', 'Login request was invalid');
      }
    } catch (e) {
      showAlertDialog(context, "Ooops...\nSomething went wrong", e.toString());
    }

    this._loggingIn = false;
  }

  String _validateInputField(String value) {
    if (value.isEmpty) {
      return 'Please fill out this field';
    }
    return null;
  }

}
