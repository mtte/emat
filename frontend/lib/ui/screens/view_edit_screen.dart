import 'dart:convert';

import 'package:emat/ui/components/progress_dialog.dart';
import 'package:emat/ui/theme/emat_theme.dart';
import 'package:emat/util/helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

abstract class ViewEditScreenState<T extends StatefulWidget> extends State<T> {
  static final titleFont = const TextStyle(
    color: EmatTheme.accent,
    fontSize: 27,
    fontWeight: FontWeight.w600,
  );

  final _formKey = GlobalKey<FormState>();

  final String name;

  bool readOnly;
  bool isNew;

  ViewEditScreenState(this.name, this.isNew) : this.readOnly = !isNew;

  void initDefaultValues();

  @override
  void initState() {
    super.initState();
    initDefaultValues();
  }

  void disposeControllers();

  @override
  void dispose() {
    disposeControllers();
    super.dispose();
  }

  Future<http.Response> createEntity();
  Future<http.Response> updateEntity();
  Future<http.Response> deleteEntity();

  void onEntitySuccessfullySaved(String response);

  void _save() async {
    if (!_formKey.currentState.validate()) {
      return;
    }

    try {
      http.Response response;
      if (this.isNew) {
        response = await createEntity();
      } else {
        response = await updateEntity();
      }

      if (response == null || response.statusCode == 200) {
        setState(() {
          this.isNew = false;
          this.readOnly = true;
          if (response != null) {
            onEntitySuccessfullySaved(response.body);
          }
        });
      } else if (response.statusCode == 400) {
        final error = json.decode(response.body);
        showAlertDialog(context, 'Saving Failed', error['message']);
      }
    } catch (e) {
      showAlertDialog(context, "Ooops...\nSomething went wrong", e.toString());
    }
  }

  void _delete() async {
    // TODO: Add confirmation dialog

    if (this.isNew) {
      return;
    }

    try {
      final response = await deleteEntity();

      if (response.statusCode == 200) {
        Navigator.pop(context);
      }
    } catch (e) {
      showAlertDialog(context, "Ooops...\nSomething went wrong", e.toString());
    }
  }

  String validateInputField(String value) {
    if (value.trim().isEmpty) {
      return 'Please fill out this field';
    }
    return null;
  }

  List<Widget> buildFormWidgets();

  @override
  Widget build(BuildContext context) {
    Widget getTitle() {
      final prefix = this.isNew ? 'Create ' : !this.readOnly ? 'Edit ' : '';
      return Text('$prefix${this.name}');
    }

    List<Widget> getActions() {
      final actions = <Widget>[];

      // Save action
      if (!this.readOnly) {
        actions.add(IconButton(
          icon: Icon(Icons.save),
          onPressed: () => _save(),
        ));
      }

      // Edit action
      if (!this.isNew && this.readOnly) {
        actions.add(IconButton(
          icon: Icon(Icons.edit),
          onPressed: () => setState(() => this.readOnly = false),
        ));
      }

      // Delete actions
      if (!this.isNew && !this.readOnly) {
        actions.add(
            IconButton(icon: Icon(Icons.delete), onPressed: () => _delete()));
      }

      return actions;
    }

    void cancelEditing() {
      initDefaultValues();
      this.readOnly = true;
    }

    List<Widget> buildForm() {
      final elements = <Widget>[];

      for (Widget widget in buildFormWidgets()) {
        elements.add(Padding(
          padding: EdgeInsets.only(bottom: 30),
          child: widget,
        ));
      }

      return elements;
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: getTitle(),
        actions: getActions(),
        leading: IconButton(
          icon: this.readOnly ? Icon(Icons.arrow_back) : Icon(Icons.close),
          onPressed: () {
            if (this.readOnly || this.isNew) {
              Navigator.pop(context);
            } else {
              setState(() => cancelEditing());
            }
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(16.0, 32.0, 16.0, 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: buildForm(),
            ),
          ),
        ),
      ),
    );
  }
}
