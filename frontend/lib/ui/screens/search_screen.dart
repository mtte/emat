import 'dart:convert';

import 'package:emat/model/item.dart';
import 'package:emat/ui/components/item_data_table.dart';
import 'package:emat/util/api.dart';
import 'package:flutter/material.dart';

class SearchScreen extends StatefulWidget {
  final String query;

  SearchScreen({Key key, @required this.query}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SearchScreenState(query);
}

class _SearchScreenState extends State<SearchScreen> {
  final String query;

  _SearchScreenState(this.query);

  Future<List<Item>> _futureSearchResult;

  Future<List<Item>> _executeSearchQuery(String query) async {
    final response = await get(context, '/search?query=$query');
    if (response.statusCode != 200) {
      print(response.body);
      throw "Error fetching items";
    }

    final List items = json.decode(response.body);
    return items.map((i) => Item.fromJson(i)).toList();
  }

  @override
  void initState() {
    super.initState();
    _futureSearchResult = _executeSearchQuery(this.query);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Search Results"),
      ),
      body: FutureBuilder<List<Item>>(
        initialData: null,
        future: this._futureSearchResult,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text(snapshot.error.toString()));
          }
          if (!snapshot.hasData) {
            return const Center(child: CircularProgressIndicator());
          }
          if (snapshot.data.isEmpty) {
            return const Center(
                child: Text(
              'No Items Found',
              style: TextStyle(fontSize: 24),
              textAlign: TextAlign.center,
            ));
          }

          return ItemDataTable(snapshot.data);
        },
      ),
    );
  }
}
