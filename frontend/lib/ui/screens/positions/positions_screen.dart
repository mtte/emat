import 'dart:async';
import 'dart:convert';

import 'package:emat/model/position.dart';
import 'package:emat/ui/components/emat_drawer.dart';
import 'package:emat/util/api.dart';
import 'package:flutter/material.dart';

class PositionsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PositionsScreenState();
}

class _PositionsScreenState extends State<PositionsScreen> {
  final _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  Future<List<Position>> _futurePositions;

  @override
  void initState() {
    super.initState();
    _futurePositions = _fetchPositions();
  }

  Future<List<Position>> _fetchPositions() async {
    final result = await get(context, '/positions');
    if (result.statusCode != 200) {
      print(result.body);
      throw "Error fetching positions";
    }

    final List positions = json.decode(result.body);
    return positions.map((p) => Position.fromJson(p)).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Positions'),
        ),
        drawer: EmatDrawer(selection: EmatDrawerSelection.positions),
        floatingActionButton: FloatingActionButton(
          onPressed: () => _editPosition(Position()),
          tooltip: 'Add Position',
          child: Icon(Icons.add_location_alt),
        ),
        body: RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: () async => setState(() {
            this._futurePositions = _fetchPositions();
          }),
          child: FutureBuilder<List<Position>>(
            future: _futurePositions,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Center(child: Text(snapshot.error.toString()));
              }
              return ListView.builder(
                padding: EdgeInsets.zero,
                itemCount: snapshot.data == null ? 0 : snapshot.data.length,
                itemBuilder: (context, i) {
                  return _buildPositionRow(snapshot.data[i]);
                },
              );
            },
          ),
        )
    );
  }

  Widget _buildPositionRow(Position position) {
    return ListTile(
      contentPadding: EdgeInsets.fromLTRB(32, 10, 32, 10),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            position.name,
            style: TextStyle(fontSize: 16),
          ),
          Text(
            position.description,
            style: Theme.of(context).textTheme.caption,
          )
        ],
      ),
      leading: Text(
        '${position.id}',
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
      ),
      onTap: () => _editPosition(position),
    );
  }

  void _editPosition(Position position) async {
    await Navigator.pushNamed(context, '/positions/edit', arguments: position);
    setState(() {
      this._futurePositions = _fetchPositions();
    });
  }

}
