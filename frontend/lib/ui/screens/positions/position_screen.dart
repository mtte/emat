import 'dart:convert';

import 'package:emat/model/position.dart';
import 'package:emat/ui/components/emat_text_field.dart';
import 'package:emat/ui/screens/view_edit_screen.dart';
import 'package:emat/ui/theme/emat_theme.dart';
import 'package:emat/util/api.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class PositionScreen extends StatefulWidget {
  final Position position;

  PositionScreen({Key key, @required this.position}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PositionScreenState(this.position);
}

class _PositionScreenState extends ViewEditScreenState<PositionScreen> {
  Position _position;

  final _nameController = TextEditingController();
  final _descriptionController = TextEditingController();

  _PositionScreenState(this._position) : super('Position', _position.id == null);

  @override
  void initDefaultValues() {
    this._nameController.text = this._position.name;
    this._descriptionController.text = this._position.description;
  }

  @override
  void disposeControllers() {
    this._nameController.dispose();
    this._descriptionController.dispose();
  }

  @override
  void onEntitySuccessfullySaved(String savedPosition) {
    this._position = Position.fromJson(json.decode(savedPosition));
  }

  @override
  Future<http.Response> deleteEntity() {
    return delete(context, '/positions/${this._position.id}');
  }

  @override
  Future<http.Response> createEntity() {
    final data = {
      'name': this._nameController.text.trim(),
    };

    final description = this._descriptionController.text.trim();
    if (description != null && description.isNotEmpty) {
      data['description'] = description;
    }

    return post(context, '/positions', data);
  }

  @override
  Future<http.Response> updateEntity() {
    final data = <String, dynamic>{};

    final name = this._nameController.text.trim();
    if (name != this._position.name) {
      data['name'] = name;
    }

    final description = this._descriptionController.text.trim();
    if (description != null &&
        description.isNotEmpty &&
        description != this._position.description) {
      data['description'] = description;
    }

    if (data.isEmpty) {
      return null;
    }

    return put(context, '/positions/${this._position.id}', data);
  }

  @override
  List<Widget> buildFormWidgets() {
    final items = <Widget>[];

    // Id
    if (!this.isNew) {
      items.add(Text(
        'Position ${this._position.id}',
        style: ViewEditScreenState.titleFont,
      ));
    }

    // Name
    items.add(EmatTextFormField(
      maxLength: 255,
      textInputAction: TextInputAction.next,
      validator: validateInputField,
      enabled: !this.readOnly,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      controller: this._nameController,
      labelText: 'Name',
    ));

    // Description
    items.add(EmatTextFormField(
      textInputAction: TextInputAction.done,
      enabled: !this.readOnly,
      controller: this._descriptionController,
      labelText: 'Description',
    ));

    return items;
  }
}