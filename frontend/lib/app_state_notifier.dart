
import 'package:emat/config/config.dart';
import 'package:emat/ui/theme/emat_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppStateNotifier extends ChangeNotifier {

  bool _isDarkMode;

  ThemeMode get themeMode {
    if (this._isDarkMode == null) {
      return ThemeMode.system;
    }
    return this._isDarkMode ? ThemeMode.dark : ThemeMode.light;
  }

  void updateTheme(bool isDarkMode) {
    this._isDarkMode = isDarkMode == null ? false : isDarkMode;
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: this._isDarkMode
          ? EmatTheme.statusBarBackgroundDark
          : EmatTheme.statusBarBackgroundLight,
      statusBarIconBrightness:
          this._isDarkMode ? Brightness.light : Brightness.dark,
    ));
    Config().setBoolValue(ConfigKey.darkTheme, _isDarkMode);
    notifyListeners();
  }
}