import 'package:emat/model/position.dart';

/// Inventory model
class InventoryList {
  int id;
  String name;
  String type;
  int inventoryId;
  Position position;

  InventoryList(
      {this.id, this.name, this.type, this.inventoryId, this.position});

  factory InventoryList.fromJson(Map<String, dynamic> json) {
    return InventoryList(
      id: json["id"],
      name: json["name"],
      type: json["type"],
      inventoryId: json["inventory"]["id"],
      position: Position.fromJson(json["position"]),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "name": this.name,
      "type": this.type,
      "inventoryId": this.inventoryId,
      "positionId": this.position.id,
    };
  }
}