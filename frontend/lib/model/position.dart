/// User model for any user
class Position {
  int id;
  String name;
  String description;

  Position({this.id, this.name, this.description});

  factory Position.fromJson(Map<String, dynamic> json) {
    return Position(
      id: json['id'],
      name: json['name'],
      description: json['description'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "name": this.name,
      "description": this.description,
    };
  }
}
