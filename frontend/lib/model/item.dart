import 'package:emat/util/helper.dart';

class Item {
  int id;
  String name;
  String description;
  int amount;
  String amountUnit;
  int inventoryListId;
  String createdBy;
  DateTime created;
  String modifiedBy;
  DateTime modified;

  Item({
      this.id,
      this.name,
      this.description,
      this.amount,
      this.amountUnit,
      this.inventoryListId,
      this.createdBy,
      this.created,
      this.modifiedBy,
      this.modified
  });

  factory Item.fromJson(Map<String, dynamic> json) {
    return Item(
      id: json["id"],
      name: json["name"],
      description: json["description"],
      amount: json["amount"],
      amountUnit: json["amountUnit"],
      inventoryListId: json["inventoryListId"],
      createdBy: json["createdBy"],
      created: getDateTimeFromJson(json["created"]),
      modifiedBy: json["modifiedBy"],
      modified: getDateTimeFromJson(json["modified"]),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "name": this.name,
      "description": this.description,
      "amount": this.amount,
      "amountUnit": this.amountUnit,
    };
  }
}
