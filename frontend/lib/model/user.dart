/// User model for any user
class User {
  int id;
  String username;
  String password;
  List<String> roles;

  User(this.id, this.username, this.roles);

  factory User.create() {
    return User(null, null, []);
  }

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      json['id'],
      json['name'],
      _getRoles(json['roles']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "username": this.username,
      "roles": this.roles,
      "password": this.password,
    };
  }
}

/// User model for the user that is currently logged in
class LoggedInUser {
  String username;
  String token;
  List<String> roles;

  LoggedInUser({this.username, this.token, this.roles});

  factory LoggedInUser.fromJson(Map<String, dynamic> json) {
    return new LoggedInUser(
        username: json['username'] ?? '',
        token: json['token'] ?? '',
        roles: _getRoles(json['roles']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "username": this.username,
      "token": this.token,
      "roles": this.roles,
    };
  }
}

List<String> _getRoles(List roles) {
  final allRoles = <String>[];
  for (var value in roles) {
    allRoles.add(value);
  }
  return allRoles;
}