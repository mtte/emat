import 'package:emat/app_state_notifier.dart';
import 'package:emat/config/config.dart';
import 'package:emat/model/user.dart';
import 'package:emat/ui/screens/home_screen.dart';
import 'package:emat/ui/screens/inventory/edit_inventory_list_screen.dart';
import 'package:emat/ui/screens/inventory/inventory_list_screen.dart';
import 'package:emat/ui/screens/inventory/item_screen.dart';
import 'package:emat/ui/screens/login_screen.dart';
import 'package:emat/ui/screens/positions/position_screen.dart';
import 'package:emat/ui/screens/positions/positions_screen.dart';
import 'package:emat/ui/screens/search_screen.dart';
import 'package:emat/ui/screens/start_screen.dart';
import 'package:emat/ui/screens/users/user_screen.dart';
import 'package:emat/ui/screens/users/users_screen.dart';
import 'package:emat/ui/theme/emat_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(EmatApp());
}

class EmatApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => EmatAppState();
}

class EmatAppState extends State<EmatApp> {
  static LoggedInUser loggedInUser;

  final AppStateNotifier _appStateNotifier = new AppStateNotifier();

  @override
  void initState() {
    super.initState();
    setupAppTheme();
  }

  void setupAppTheme() async {
    final value = await Config().getBoolValue(ConfigKey.darkTheme);
    _appStateNotifier.updateTheme(value);
  }

  Route<dynamic> _generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/search':
        final query = settings.arguments;
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => SearchScreen(query: query),
        );
      case '/list':
        final list = settings.arguments;
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => InventoryListScreen(list: list),
        );
      case '/list/edit':
        final list = settings.arguments;
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => EditInventoryListScreen(list: list),
        );
      case '/item/edit':
        final item = settings.arguments;
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => ItemScreen(item: item),
        );
      case '/users/edit':
        final user = settings.arguments;
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => UserScreen(user: user),
        );
      case '/positions/edit':
        final position = settings.arguments;
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => PositionScreen(position: position),
        );
      default:
        return null;
    }
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return ChangeNotifierProvider<AppStateNotifier>(
      create: (_) => _appStateNotifier,
      child: Consumer<AppStateNotifier>(
        builder: (context, appState, child) {
          return MaterialApp(
            title: 'emat',
            debugShowCheckedModeBanner: false,
            theme: EmatTheme.lightTheme,
            darkTheme: EmatTheme.darkTheme,
            themeMode: appState.themeMode,
            initialRoute: '/',
            routes: {
              '/': (context) => StartScreen(),
              '/home': (context) => HomeScreen(),
              '/login': (context) => LoginScreen(),
              '/users': (context) => UsersScreen(),
              '/positions': (context) => PositionsScreen(),
            },
            onGenerateRoute: _generateRoute,
          );
        },
      ),
    );
  }
}
