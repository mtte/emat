
import 'package:shared_preferences/shared_preferences.dart';

enum ConfigKey {
  darkTheme, // bool
  lastLoggedInUser, // User as JSON
}

class Config {

  Future<SharedPreferences> _sharedPreferences;

  Config() : this._sharedPreferences = SharedPreferences.getInstance();

  void removeValue(ConfigKey key) async {
    await this._sharedPreferences
      ..remove(key.toString());
  }

  Future<dynamic> getValue(ConfigKey key) async {
    var prefs = await this._sharedPreferences;
    return prefs.get(key.toString());
  }

  Future<String> getStringValue(ConfigKey key) async {
    var prefs = await this._sharedPreferences;
    return prefs.getString(key.toString());
  }

  void setStringValue(ConfigKey key, String value) async {
    await this._sharedPreferences
      ..setString(key.toString(), value);
  }

  Future<bool> getBoolValue(ConfigKey key) async {
    var prefs = await this._sharedPreferences;
    return prefs.getBool(key.toString());
  }

  void setBoolValue(ConfigKey key, bool value) async {
    await this._sharedPreferences
      ..setBool(key.toString(), value);
  }

  Future<int> getIntValue(ConfigKey key) async {
    var prefs = await this._sharedPreferences;
    return prefs.getInt(key.toString());
  }

  void setIntValue(ConfigKey key, int value) async {
    await this._sharedPreferences
      ..setInt(key.toString(), value);
  }

  Future<double> getDoubleValue(ConfigKey key) async {
    var prefs = await this._sharedPreferences;
    return prefs.getDouble(key.toString());
  }

  void setDoubleValue(ConfigKey key, double value) async {
    await this._sharedPreferences
      ..setDouble(key.toString(), value);
  }

  Future<List<String>> getStringListValue(ConfigKey key) async {
    var prefs = await this._sharedPreferences;
    return prefs.getStringList(key.toString());
  }

  void setStringListValue(ConfigKey key, List<String> value) async {
    await this._sharedPreferences
      ..setStringList(key.toString(), value);
  }

}
