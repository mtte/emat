import 'package:emat/config/config.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SecureConfig {

  FlutterSecureStorage _secureStorage;

  SecureConfig() : this._secureStorage = FlutterSecureStorage();

  Future<String> getValue(ConfigKey key) async {
    return await this._secureStorage.read(key: key.toString());
  }

  Future<void> setValue(ConfigKey key, String value) {
    return this._secureStorage.write(key: key.toString(), value: value);
  }

  Future<void> removeValue(ConfigKey key) {
    return this._secureStorage.delete(key: key.toString());
  }

}
