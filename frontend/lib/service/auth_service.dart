import 'package:emat/config/config.dart';
import 'package:emat/config/secure_config.dart';
import 'package:emat/main.dart';
import 'package:emat/model/user.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void logout(BuildContext context) {
  EmatAppState.loggedInUser = null;
  SecureConfig().removeValue(ConfigKey.lastLoggedInUser);
  Navigator.pushNamedAndRemoveUntil(
      context, '/login', (route) => false);
}

bool isAdmin(LoggedInUser user) {
  return user.roles.contains(Role.ADMIN.name)
       || user.roles.contains(Role.SUPER_USER.name);
}

bool isSuperAdmin(LoggedInUser user) {
  return user.roles.contains(Role.SUPER_USER.name);
}

enum Role {
  ADMIN,
  SUPER_USER,
}

extension RoleFunctionality on Role {

  /// The name of this role as specified in the enum (without preceding class)
  String get name => describeEnum(this);

  /// The UI display name of a [Role]
  String get displayName {
    switch (this) {
      case Role.ADMIN:
        return 'Administrator';
        break;
      case Role.SUPER_USER:
        return 'Super Admin';
      default:
        throw 'This enum value does not have a display name specified!';
    }
  }

  /// Static helper to get a [Role] by it's [String] name
  Role getByName(String name) {
    for (Role role in Role.values) {
      if (role.name == name) {
        return role;
      }
    }
    throw "No Role with this name existing";
  }

}
