
import 'dart:convert';
import 'dart:io';

import 'package:emat/main.dart';
import 'package:emat/util/constants.dart';
import 'package:emat/util/helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

Future<http.Response> post(
    BuildContext context, String path, Map<String, dynamic> data) async {

  final response = await _postInternal(path, data);

  _handleErrors(context, response);

  return response;
}

Future<http.Response> _postInternal(
    String path, Map<String, dynamic> data) async {

  final url = _getUrl(path);

  return http.post(url, headers: _getHeaders(), body: json.encode(data));
}

Future<http.Response> get(BuildContext context, String path) async {
  final response = await _getInternal(path);

  _handleErrors(context, response);

  return response;
}

Future<http.Response> _getInternal(String path) async {
  return http.get(_getUrl(path), headers: _getHeaders());
}

Future<http.Response> delete(BuildContext context, String path) async {
  final response = await _deleteInternal(path);

  _handleErrors(context, response);

  return response;
}

Future<http.Response> _deleteInternal(String path) async {
  return http.delete(_getUrl(path), headers: _getHeaders());
}

Future<http.Response> put(
    BuildContext context, String path, Map<String, dynamic> data) async {

  final response = await _putInternal(path, data);

  _handleErrors(context, response);

  return response;
}

Future<http.Response> _putInternal(
    String path, Map<String, dynamic> data) async {

  final body = json.encode(data);

  return http.put(_getUrl(path), headers: _getHeaders(), body: body);
}

String _getUrl(String path) {
  return API_URL + path;
}

Map<String, String> _getHeaders() {
  final headers = {HttpHeaders.contentTypeHeader: 'application/json'};

  final user = EmatAppState.loggedInUser;
  if (user != null) {
    headers[HttpHeaders.authorizationHeader] = 'Bearer ${user.token}';
  }

  return headers;
}

void _handleErrors(BuildContext context, http.Response response) async {
  if (response.statusCode == 401) {
    // No longer authorized
    Navigator.pushNamed(context, '/login');
  } else if (response.statusCode == 403) {
    // Forbidden action
    await showAlertDialog(context, 'Not Authorized',
        'You do not have the permissions required for this action');
  } else if (response.statusCode == 500) {
    await showAlertDialog(context,
        'Internal Server Error', "The server returned an error");
  }
}
