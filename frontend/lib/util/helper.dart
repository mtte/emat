import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';

import '../app_state_notifier.dart';

//helper method to show alert dialog
showAlertDialog(BuildContext context, String title, String content) async {
  Widget okButton = FlatButton(
    child: Text("Okay"),
    onPressed: () {
      Navigator.pop(context);
    },
  );
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(content),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  await showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

pushReplacement(BuildContext context, Widget destination) {
  Navigator.of(context).pushReplacement(
      new MaterialPageRoute(builder: (context) => destination));
}

push(BuildContext context, Widget destination) {
  Navigator.of(context)
      .push(new MaterialPageRoute(builder: (context) => destination));
}

pushAndRemoveUntil(BuildContext context, Widget destination, bool predict) {
  Navigator.of(context).pushAndRemoveUntil(
      MaterialPageRoute(builder: (context) => destination),
      (Route<dynamic> route) => predict);
}

Future<String> getAppVersionInfo() async {
  PackageInfo packageInfo = await PackageInfo.fromPlatform();
  String appName = packageInfo.appName;
  String version = packageInfo.version;
  return '$appName $version';
}

bool isUsingDarkTheme(BuildContext context) {
  final themeMode = Provider.of<AppStateNotifier>(context).themeMode;
  final brightness = MediaQuery.of(context).platformBrightness;
  return (themeMode == ThemeMode.dark) ||
      (themeMode == ThemeMode.system && brightness == Brightness.dark);
}

DateTime getDateTimeFromJson(Map<String, dynamic> json) {
  return DateTime(
      json["date"]["year"],
      json["date"]["month"],
      json["date"]["day"],
      json["time"]["hour"],
      json["time"]["minute"],
      json["time"]["second"],
      0,
      (json["time"]["nano"] / 1000).round());
}