package me.mtte.code.emat

import me.mtte.code.emat.database.Database
import me.mtte.code.emat.database.H2Database
import me.mtte.code.emat.security.SecurityConfigFactory
import spark.Spark.path
import spark.kotlin.after
import spark.kotlin.get

val database: Database = H2Database()
val securityConfig = SecurityConfigFactory(Config.JWT_SALT.get()).build()

fun main() {
    setupPort(Config.SERVER_PORT.getOrNull())
    removeTrailingSlashes()
    handleExceptions()
    handleJsonPayload()
    enableCORS(Config.CORS_ALLOWED_ORIGINS.get(), Config.CORS_ALLOWED_METHODS.get(),
            Config.CORS_ALLOWED_HEADERS.get(), Config.CORS_ALLOW_CREDENTIALS.get())

    get("/ruok") {
        "imok"
    }

    path("/api", Api())

    after() {
        response.gzip()
        response.json()
    }
}
