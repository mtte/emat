package me.mtte.code.emat.services

import me.mtte.code.emat.database

import me.mtte.code.emat.database.schema.emat.tables.Inventory.INVENTORY
import me.mtte.code.emat.model.Inventory
import java.util.*

object InventoryService {

    fun getAllInventories(): Set<Inventory> {
        return database.get().selectFrom(INVENTORY).fetch().map { Inventory(it) }.toSet()
    }

    fun getInventory(id: Int): Optional<Inventory> {
        return database.get().fetchOptional(INVENTORY, INVENTORY.ID.eq(id)).map { Inventory(it) }
    }

    fun createInventory(name: String, description: String): Inventory {
        val inventory = database.get().newRecord(INVENTORY)
        inventory.name = name
        inventory.description = description
        inventory.store()
        return Inventory(inventory)
    }

    fun deleteInventory(id: Int): Boolean {
        return database.get().delete(INVENTORY).where(INVENTORY.ID.eq(id)).execute() == 1
    }

    fun updateInventory(id: Int?, name: String?, description: String?): Optional<Inventory> {
        val inventory = database.get().fetchOne(INVENTORY, INVENTORY.ID.eq(id)) ?: return Optional.empty()
        if (name != null) {
            inventory.name = name
        }
        if (description != null) {
            inventory.description = description
        }
        inventory.store()
        return Optional.of(Inventory(inventory))
    }

    fun inventoryExists(id: Int): Boolean {
        return database.get().fetchExists(database.get().selectFrom(INVENTORY).where(INVENTORY.ID.eq(id)))
    }

}