package me.mtte.code.emat.services

import me.mtte.code.emat.database
import me.mtte.code.emat.database.schema.emat.tables.Item.ITEM
import me.mtte.code.emat.model.Item
import kotlin.streams.toList

object SearchService {

    fun search(query: String): List<Item> {
        val result = database.get().selectFrom(ITEM)
                .where(ITEM.NAME.likeIgnoreCase("%$query%")
                        .or(ITEM.DESCRIPTION.likeIgnoreCase("%$query%")))
                .fetchStream()
        return result.map { Item(it) }.toList()
    }

}