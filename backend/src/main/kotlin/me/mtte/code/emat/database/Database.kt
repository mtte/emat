package me.mtte.code.emat.database

import org.jooq.DSLContext

interface Database {
    /**
     * Get the dsl context for the database.
     * @return The [DSLContext].
     */
    fun get(): DSLContext
}
