package me.mtte.code.emat.services

import me.mtte.code.emat.database
import me.mtte.code.emat.database.schema.emat.tables.Authorization.AUTHORIZATION
import me.mtte.code.emat.security.Role
import org.jooq.DSLContext

object AuthService {

    fun deleteAllAuthorizations(transaction: DSLContext, userId: Int): Boolean {
        val deleted = transaction.delete(AUTHORIZATION)
            .where(AUTHORIZATION.USER.eq(userId))
            .execute()
        return deleted > 0
    }

    fun addAuthorization(userId: Int, role: Role) {
        val auth = database.get().newRecord(AUTHORIZATION)
        auth.user = userId
        auth.role = role.toString()
        auth.store()
    }

    fun getRoles(userId: Int): Set<Role> {
        return database.get().select(AUTHORIZATION.ROLE)
            .from(AUTHORIZATION)
            .where(AUTHORIZATION.USER.eq(userId))
            .fetch() { Role.valueOf(it.get(AUTHORIZATION.ROLE)) }.toSet()
    }

    fun deleteAuthorization(userId: Int, role: String): Boolean {
        return database.get().delete(AUTHORIZATION)
            .where(AUTHORIZATION.USER.eq(userId).and(AUTHORIZATION.ROLE.eq(role)))
            .execute() == 1
    }

}
