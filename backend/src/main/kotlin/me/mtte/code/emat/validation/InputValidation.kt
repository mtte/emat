package me.mtte.code.emat.validation

import me.mtte.code.emat.convertToInt
import me.mtte.code.emat.getParameter
import me.mtte.code.emat.halt
import me.mtte.code.emat.responses.MessageResponse
import spark.Request
import spark.Response

class InputValidation(
    request: Request,
    private val response: Response,
    private val parameterName: String,
    private val displayValue: String?,
    validator: Validator<String?>
) : Validation<String?>(request.getParameter(parameterName), validator) {

    companion object {

        fun validateParameter(request: Request, response: Response, parameterName: String, validator: Validator<String?>): String? {
            return validateParameter(request, response, parameterName, request.getParameter(parameterName), validator)
        }

        fun validateParameter(request: Request, response: Response, parameterName: String, displayValue: String?, validator: Validator<String?>): String? {
            val iv = InputValidation(
                request,
                response,
                parameterName,
                displayValue,
                validator
            )
            iv.haltWhenFailed()
            return iv.value
        }

        fun validateIntParameter(request: Request, response: Response, parameterName: String):Int {
            return validateIntParameter(request, response, parameterName, request.getParameter(parameterName))
        }

        fun validateIntParameter(request: Request, response: Response, parameterName: String, displayValue: String?): Int {
            val int = validateParameter(request, response, parameterName, displayValue, StandardValidators.nonNullString().and(
                StandardValidators.isInt()
            ))
            return convertToInt(int)!!
        }

    }

    constructor(request: Request, response: Response, parameterName: String, validator: Validator<String?>)
            : this(request, response, parameterName, request.getParameter(parameterName), validator)

    fun haltWhenFailed() {
        if (failed()) {
            response.halt(400,
                MessageResponse("Invalid parameter: '${parameterName}' with value '${displayValue}'. ${error.get()}")
            )
        }
    }

}