package me.mtte.code.emat.controllers

import me.mtte.code.emat.getLogger
import me.mtte.code.emat.halt
import me.mtte.code.emat.model.User
import me.mtte.code.emat.responses.LoginResponse
import me.mtte.code.emat.responses.MessageResponse
import me.mtte.code.emat.security.Password
import me.mtte.code.emat.services.UserService
import me.mtte.code.emat.validation.InputValidation.Companion.validateParameter
import me.mtte.code.emat.validation.StandardValidators.nonNullString
import me.mtte.code.emat.validation.StandardValidators.notEmpty
import me.mtte.code.emat.validation.and
import org.pac4j.core.profile.CommonProfile
import org.pac4j.jwt.config.signature.SecretSignatureConfiguration
import org.pac4j.jwt.profile.JwtGenerator
import spark.Request
import spark.Response
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

object LoginController {

    fun handleLogin(request: Request, response: Response): Any {
        // Validation
        val username = validateParameter(request, response, "username", nonNullString().and(notEmpty()))!!
        val password = validateParameter(request, response, "password", "*****", nonNullString().and(notEmpty()))!!

        // Authenticate
        val user = authenticate(username, password)
        if (user.isEmpty) {
            getLogger(this).info("Login attempt from {} failed: {}", request.ip(), username)
            response.halt(401, MessageResponse("Login credentials invalid"))
        }

        // Create profile and token and respond
        val profile = createProfile(user.get())
        val token = generateJwtToken(profile)
        getLogger(this ).info("User {} logged in successfully", username)
        return LoginResponse(token, user.get().name, user.get().roles)
    }

    private fun authenticate(username: String, password: String): Optional<User> {
        val user: Optional<User> = UserService.getUserWithPasswordByName(username)
        return user.filter { u: User ->
            Password.verifyPassword(
                password.toCharArray(),
                u.password!!.toCharArray()
            )
        }
    }

    private fun createProfile(user: User): CommonProfile {
        val profile = CommonProfile()
        profile.id = user.id.toString()
        profile.clientName = user.name
        profile.roles = user.roles.map { it.toString() }.toSet()
        return profile
    }

    private fun generateJwtToken(profile: CommonProfile): String {
        val generator =
            JwtGenerator<CommonProfile>(SecretSignatureConfiguration(System.getenv("JWT_SALT")))
        generator.expirationTime = Date.from(Instant.now().plus(30, ChronoUnit.HOURS))
        return generator.generate(profile)
    }

}