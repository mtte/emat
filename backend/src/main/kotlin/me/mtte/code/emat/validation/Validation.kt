package me.mtte.code.emat.validation

import java.util.*
import java.util.function.Predicate

typealias ValidationError = String

typealias Validator<T> = (value: T) -> Optional<ValidationError>

fun <T> Validator<T>.and(validator: Validator<T>): Validator<T> {
    return {
        val result = invoke(it)
        if (result.isPresent) result
        else validator.invoke(it)
    }
}

fun <T> Validator<T>.or(validator: Validator<T>): Validator<T> {
    return {
        val result = invoke(it);
        if (result.isPresent) validator.invoke(it)
        else Optional.empty()
    }
}

open class Validation<T>(val value: T, validator: Validator<T>) {

    val error: Optional<ValidationError> = validator.invoke(value)

    fun failed(): Boolean = error.isPresent

    fun getError(): ValidationError = error.get()

}

class BooleanValidator<T>(private val predicate: Predicate<T>, private val errorMessage: String) : Validator<T> {

    override fun invoke(value: T): Optional<ValidationError> =
        if (this.predicate.test(value)) Optional.empty() else Optional.of(errorMessage)

}
