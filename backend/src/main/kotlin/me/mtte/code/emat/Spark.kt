package me.mtte.code.emat

import com.google.gson.*
import com.google.gson.reflect.TypeToken
import me.mtte.code.emat.model.User
import me.mtte.code.emat.responses.MessageResponse
import me.mtte.code.emat.services.UserService
import org.pac4j.core.context.WebContext
import org.pac4j.core.profile.CommonProfile
import org.pac4j.core.profile.ProfileManager
import org.pac4j.sparkjava.SparkWebContext
import spark.Filter
import spark.Request
import spark.Response
import spark.Spark
import spark.Spark.exception
import spark.Spark.notFound
import spark.kotlin.*
import java.util.*


/**
 * Get a parameter of the request.
 * @param name The name of the parameter
 * @return The parameter's value
 */
fun Request.getParameter(name: String): String? {
    val param = params(name)
    if (param != null && param.isNotEmpty()) {
        return param
    }

    val queryParam = queryParams(name)
    if (queryParam != null && queryParam.isNotEmpty()) {
        return queryParam
    }

    val attribute: Any? = attribute(name)
    if (attribute != null && attribute is String && attribute.isNotEmpty()) {
        return attribute
    }

    return null
}

/**
 * Get a parameter of the request as [Int].
 * @param name The name of the parameter
 * @return The parameter's value as integer
 */
fun Request.getParameterAsInt(name: String): Int? {
    val value = getParameter(name)
    return convertToInt(value)
}

/**
 * Get a parameter that is an array and was supplied by JSON.
 * @param name The name of the parameter
 * @param T The type of object in the list
 * @return The JSON array as list using the type T
 */
fun <T> Request.getArrayParameter(name: String): List<T>? {
    val attribute: Any? = attribute(name)
    if (attribute != null && attribute is JsonArray) {
        val listType = object : TypeToken<List<T>>() {}.type
        return Gson().fromJson<List<T>>(attribute, listType)
    }
    return null;
}

/**
 * Get the value of the request's id parameter.
 * @return The value of the id parameter, if the request has one
 */
fun Request.getId(): Int? = getParameterAsInt(":id")

/**
 * Use gzip for this response.
 */
fun Response.gzip() = header("Content-Encoding", "gzip")

/**
 * This response is of type application/json.
 */
fun Response.json() = type("application/json; charset=utf-8")

/**
 * Immediately stop the request.
 * @param code The response status
 * @param response The response message
 */
fun Response.halt(code: Int, response: me.mtte.code.emat.responses.Response) {
    gzip()
    json()
    halt(code, response.toJson())
    Spark.halt()
}

fun getUserId(request: Request, response: Response): Optional<Int> {
    val profile = getProfile(request, response)
    return profile.map { commonProfile -> convertToInt(commonProfile.id) }
}

fun getUser(request: Request, response: Response): Optional<User> {
    val profile = getProfile(request, response)
    if (profile.isEmpty) {
        return Optional.empty()
    }
    val theProfile = profile.get()
    val id = convertToInt(theProfile.id) ?: return Optional.empty()
    return UserService.getUser(id)
}

fun getProfile(request: Request, response: Response): Optional<CommonProfile> {
    val context: WebContext = SparkWebContext(request, response)
    val manager = ProfileManager<CommonProfile>(context)
    return manager[false]
}

fun removeTrailingSlashes() {
    before(Filter { request, response ->
        val path = request.pathInfo()
        if (path.endsWith("/")) {
            response.redirect(path.substring(0, path.length - 1))
        }
    })
}

fun enableCORS(origin: String, methods: String, headers: String, credentials: String) {
    options("/*") {
        val accessControlRequestHeaders = request.headers("Access-Control-Request-Headers")
        if (accessControlRequestHeaders != null) {
            response.header("Access-Control-Allow-Headers", accessControlRequestHeaders)
        }

        val accessControlRequestMethod = request.headers("Access-Control-Request-Method")
        if (accessControlRequestMethod != null) {
            response.header("Access-Control-Allow-Methods", accessControlRequestMethod)
        }

        "OK"
    }

    before(Filter { _, response ->
        response.header("Access-Control-Allow-Origin", origin)
        response.header("Access-Control-Request-Method", methods)
        response.header("Access-Control-Allow-Headers", headers)
        response.header("Access-Control-Allow-Credentials", credentials)
    })
}

fun setupPort(port: String?) {
    val portInt = convertToInt(port)
    if (portInt != null) {
        port(portInt)
    }
}

fun handleJsonPayload() {
    before(Filter { request, _ ->
        val body = request.body()
        try {
            val parsed = JsonParser.parseString(body)
            if (parsed.isJsonObject) {
                val payload = parsed.asJsonObject
                for (key in payload.keySet()) {
                    val jsonElement = payload.get(key)
                    if (jsonElement.isJsonPrimitive) {
                        request.attribute(key, jsonElement.asString)
                    } else if (jsonElement.isJsonArray) {
                        request.attribute(key, jsonElement.asJsonArray)
                    }
                }
            }
        } catch (e: JsonParseException) {
            // Ignore, probably not a json payload
        }
    })
}

fun handleExceptions() {
    exception(Exception::class.java) { exception, _, response ->
        getLogger("EXCEPTION-LOGGER").error("Spark request caused exception", exception)
        response.status(500)
        response.body(toJson(MessageResponse("Internal Server Error")))
    }

    internalServerError {
        response.json()
        toJson(MessageResponse("Internal Server Error"))
    }

    notFound() { _: Request, response: Response ->
        response.json()
        toJson(MessageResponse("Not Found"))
    }
}
