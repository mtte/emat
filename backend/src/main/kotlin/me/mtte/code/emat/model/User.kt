package me.mtte.code.emat.model

import me.mtte.code.emat.database.schema.emat.tables.records.UserRecord
import me.mtte.code.emat.security.Role

/**
 * Data class representing a user. The password is normally not set.
 */
data class User(val id: Int, val name: String, var roles: Set<Role>, var password: String?) {
    constructor(userRecord: UserRecord) : this(userRecord.id, userRecord.name, emptySet(), null)
}