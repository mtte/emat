package me.mtte.code.emat.controllers

import me.mtte.code.emat.isValidEnumValue
import me.mtte.code.emat.responses.SuccessResponse
import me.mtte.code.emat.security.Role
import me.mtte.code.emat.services.AuthService
import me.mtte.code.emat.validation.BooleanValidator
import me.mtte.code.emat.validation.InputValidation.Companion.validateIntParameter
import me.mtte.code.emat.validation.InputValidation.Companion.validateParameter
import me.mtte.code.emat.validation.StandardValidators.nonNullString
import me.mtte.code.emat.validation.StandardValidators.notEmpty
import me.mtte.code.emat.validation.Validator
import me.mtte.code.emat.validation.and
import spark.Request
import spark.Response
import java.util.function.Predicate

object AuthController {

    private val roleValidation: Validator<String?>
        get() {
            val roleExists = BooleanValidator<String?>(Predicate { isValidEnumValue<Role>(it) }, "Role does not exist")
            return nonNullString().and(notEmpty()).and(roleExists)
        }

    fun addRole(request: Request, response: Response): Any {
        val userId = validateIntParameter(request, response, "id")
        val roleString = validateParameter(request, response, "role", roleValidation)!!

        val role = Role.valueOf(roleString)

        if (AuthService.getRoles(userId).contains(role)) {
            return SuccessResponse()
        }

        AuthService.addAuthorization(userId, role)

        return SuccessResponse()
    }

    fun deleteRole(request: Request, response: Response): Any {
        val userId = validateIntParameter(request, response, "id")
        val role = validateParameter(request, response, "role", roleValidation)!!

        AuthService.deleteAuthorization(userId, role)

        return SuccessResponse()
    }

    fun checkAuth(): Any {
        // Always return a success response as authorization would have already failed before
        return SuccessResponse()
    }

}