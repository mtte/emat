package me.mtte.code.emat.services

import me.mtte.code.emat.database
import me.mtte.code.emat.database.schema.emat.tables.InventoryList.INVENTORY_LIST
import me.mtte.code.emat.model.InventoryList
import java.util.*

object InventoryListService {

    fun getAllInventoryLists(): Set<InventoryList> {
        return database.get().selectFrom(INVENTORY_LIST).fetch().map { InventoryList(it) }.toSet()
    }

    fun getInventoryList(id: Int): Optional<InventoryList> {
        return database.get().fetchOptional(INVENTORY_LIST, INVENTORY_LIST.ID.eq(id)).map { InventoryList(it) }
    }

    fun createInventoryList(name: String, type: String, positionId: Int, inventoryId: Int): InventoryList {
        val inventoryList = database.get().newRecord(INVENTORY_LIST)
        inventoryList.name = name
        inventoryList.type = type
        inventoryList.positionId = positionId
        inventoryList.inventoryId = inventoryId
        inventoryList.store()
        return InventoryList(inventoryList)
    }

    fun deleteInventoryList(id: Int): Boolean {
        return database.get().delete(INVENTORY_LIST).where(INVENTORY_LIST.ID.eq(id)).execute() == 1
    }

    fun updateInventoryList(id: Int?, name: String?, type: String?, positionId: Int?): Optional<InventoryList> {
        val inventoryList = database.get().fetchOne(INVENTORY_LIST, INVENTORY_LIST.ID.eq(id)) ?: return Optional.empty()
        if (name != null) {
            inventoryList.name = name
        }
        if (type != null) {
            inventoryList.type = type
        }
        if (positionId != null) {
            inventoryList.positionId = positionId
        }
        inventoryList.store()
        return Optional.of(InventoryList(inventoryList))
    }

    fun inventoryListExists(id: Int): Boolean {
        return database.get().fetchExists(database.get().selectFrom(INVENTORY_LIST).where(INVENTORY_LIST.ID.eq(id)))
    }

}