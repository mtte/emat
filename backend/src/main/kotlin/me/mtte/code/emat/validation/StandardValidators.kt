package me.mtte.code.emat.validation

import me.mtte.code.emat.convertToInt
import java.util.*
import java.util.function.Predicate

object StandardValidators {

    fun <T> nonNull(): Validator<T> {
        return  {
            if (it == null) Optional.of("Object cannot be null") else Optional.empty()
        }
    }

    fun nonNullString(): Validator<String?> {
        return nonNull()
    }

    fun notEmpty(): Validator<String?> {
        return {
            if (it!!.trim().isBlank()) Optional.of("Value must not be empty") else Optional.empty()
        }
    }

    fun maxLength(maxLength: Int): Validator<String?> {
        return {
            when {
                it == null -> Optional.empty()
                it.length > maxLength -> Optional.of("Max length of $maxLength exceeded")
                else -> Optional.empty()
            }
        }
    }

    fun minLength(minLength: Int): Validator<String?> {
        return {
            if (it!!.length < minLength) Optional.of("Min length of $minLength not reached") else Optional.empty()
        }
    }

    fun isBoolean(): Validator<String?> {
        return BooleanValidator(
            Predicate { "true".equals(it, true) || "false".equals(it, true) },
            "Not a boolean (true/false)"
        )
    }

    fun isInt(): Validator<String?> {
        return {
            if (convertToInt(it) == null) Optional.of(
                "'$it' is not a valid number"
            ) else Optional.empty()
        }
    }

    inline fun <reified T : Enum<T>>  isValidEnumValue(): Validator<String?> {
        return {
            if (me.mtte.code.emat.isValidEnumValue<T>(it)) {
                Optional.of("The given value $it of ${T::class.java.simpleName} does not exist")
            } else {
                Optional.empty()
            }
        }
    }

}