package me.mtte.code.emat.security

enum class Role {
    USER,
    ADMIN,
    SUPER_USER;

}
