package me.mtte.code.emat.controllers

import me.mtte.code.emat.*
import me.mtte.code.emat.responses.MessageResponse
import me.mtte.code.emat.responses.Response
import me.mtte.code.emat.responses.SuccessResponse
import me.mtte.code.emat.services.ItemService
import me.mtte.code.emat.validation.InputValidation.Companion.validateIntParameter
import me.mtte.code.emat.validation.InputValidation.Companion.validateParameter
import me.mtte.code.emat.validation.StandardValidators.maxLength
import me.mtte.code.emat.validation.StandardValidators.nonNullString
import me.mtte.code.emat.validation.StandardValidators.notEmpty
import me.mtte.code.emat.validation.and
import spark.Request
import spark.Route
import spark.RouteGroup
import spark.Spark
import spark.Spark.get

object ItemController : RouteGroup {

    private val nameValidation = nonNullString().and(maxLength(255)).and(notEmpty());
    private val unitValidation = maxLength(25)

    override fun addRoutes() {
        // Get all items
        get("", Route(this::getAllItems), json())

        // Create item
        Spark.post("", Route(this::createItem), json())

        // Get single item
        get("/:id", Route(this::getItem), json())

        // Update item
        Spark.put("/:id", Route(this::updateItem), json())

        // Delete item
        Spark.delete("/:id", Route(this::deleteItem), json())
    }

    private fun getAllItems(request: Request, response: spark.Response): Any? {
        val id = validateIntParameter(request, response, ":listId")

        return ItemService.getAllItems(id)
    }

    private fun getItem(request: Request, response: spark.Response): Any? {
        val id = request.getId() ?: return Response.returnInvalidIdError(request, response)
        val item = ItemService.getItem(id)
        if (item.isEmpty) {
            Response.returnInvalidIdError(request, response)
        }
        return item.get()
    }

    private fun createItem(request: Request, response: spark.Response): Any? {
        val name = validateParameter(request, response, "name", nameValidation)
        val description = request.getParameter("description") ?: ""
        val amount = validateIntParameter(request, response, "amount")
        val unit = validateParameter(request, response, "unit", unitValidation) ?: ""

        val userId = getUserId(request, response).orElse(null)
                ?: return Response.invalidRequestError(response, "No user profile found")

        val inventoryListId = validateIntParameter(request, response, ":listId")

        return ItemService.createItem(name!!, description, amount, unit, userId, inventoryListId)
    }

    private fun updateItem(request: Request, response: spark.Response): Any? {
        val name = request.getParameter("name")
        val description = request.getParameter("description")
        val amount = request.getParameter("amount")
        val unit = request.getParameter("unit")

        val userId = getUserId(request, response).orElse(null)
                ?: return Response.invalidRequestError(response, "No user profile found")

        if (name != null) {
            validateParameter(request, response, "name", nameValidation)
        }
        if (unit != null) {
            validateParameter(request, response, "unit", unitValidation)
        }
        var amountInt: Int? = null
        if (amount != null) {
            amountInt = validateIntParameter(request, response, "amount")
        }

        val item = ItemService.updateItem(request.getId(), name, description, amountInt, unit, userId)
        if (item.isEmpty) {
            Response.returnInvalidIdError(request, response)
        }
        return item.get()
    }

    private fun deleteItem(request: Request, response: spark.Response): Any? {
        val id = request.getId() ?: return Response.returnInvalidIdError(request, response)

        if (!ItemService.deleteItem(id)) {
            response.halt(500, MessageResponse("Error while deleting item with id $id"))
        }

        return SuccessResponse()
    }

}