package me.mtte.code.emat.database

class InvalidDatabaseStateException(private val reason: String) : RuntimeException("Invalid database state: $reason") {}
