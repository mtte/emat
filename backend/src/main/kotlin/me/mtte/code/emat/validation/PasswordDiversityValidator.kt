package me.mtte.code.emat.validation

import java.util.*
import java.util.regex.Pattern

class PasswordDiversityValidator : Validator<String?> {

    companion object {
        private val LOWER_CASE_PATTERN = Pattern.compile("[a-z]")
        private val UPPER_CASE_PATTERN = Pattern.compile("[A-Z]")
        private val DIGIT_PATTERN = Pattern.compile("[0-9]")
        private val SPECIAL_CHARS_PATTERN = Pattern.compile("[^a-zA-Z0-9]*")
    }

    override fun invoke(password: String?): Optional<ValidationError> {
        var categoriesMet = 0

        if (LOWER_CASE_PATTERN.matcher(password).find()) {
            categoriesMet++
        }
        if (UPPER_CASE_PATTERN.matcher(password).find()) {
            categoriesMet++
        }
        if (DIGIT_PATTERN.matcher(password).find()) {
            categoriesMet++
        }
        if (SPECIAL_CHARS_PATTERN.matcher(password).find()) {
            categoriesMet++
        }

        if (categoriesMet < 3) {
            return Optional.of(
                "Password has to meet at least 3 of the following categories: " +
                        "lower case, upper case, numbers, special characters"
            )
        }

        return Optional.empty()
    }

}