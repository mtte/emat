package me.mtte.code.emat.database

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.impl.DSL

class H2Database : Database {

    private val dsl = init()

    override fun get(): DSLContext {
        return dsl
    }

    /**
     * Setup the DSL context for jOOQ
     * @return jOOQ DSL context to access the db
     */
    private fun init(): DSLContext {
        val config = HikariConfig("database.properties")
        config.poolName = "H2Database-CP"
        val dataSource = HikariDataSource(config)
        return DSL.using(dataSource, SQLDialect.H2)
    }

}
