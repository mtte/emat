package me.mtte.code.emat

import java.lang.RuntimeException

enum class Config(val required: Boolean = true) {

    JWT_SALT,

    CORS_ALLOWED_ORIGINS,
    CORS_ALLOWED_METHODS,
    CORS_ALLOWED_HEADERS,
    CORS_ALLOW_CREDENTIALS,

    SERVER_PORT(false),
    ;

    fun get(): String {
        return getOrNull()!!
    }

    fun getOrNull(): String? {
        return System.getenv(name)
    }

    companion object {
        init {
            values().forEach {
                if (it.required) {
                    val value = it.getOrNull()
                    if (value.isNullOrBlank()) {
                        throw ConfigException(it)
                    }
                }
            }
        }
    }

    private class ConfigException(private val value: Config): RuntimeException("Config value ${value.name} is not specified!")

}