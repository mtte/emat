package me.mtte.code.emat.controllers

import me.mtte.code.emat.getId
import me.mtte.code.emat.getParameter
import me.mtte.code.emat.halt
import me.mtte.code.emat.json
import me.mtte.code.emat.responses.MessageResponse
import me.mtte.code.emat.responses.Response
import me.mtte.code.emat.responses.SuccessResponse
import me.mtte.code.emat.services.InventoryListService
import me.mtte.code.emat.services.InventoryService
import me.mtte.code.emat.services.PositionService
import me.mtte.code.emat.validation.InputValidation.Companion.validateIntParameter
import me.mtte.code.emat.validation.InputValidation.Companion.validateParameter
import me.mtte.code.emat.validation.StandardValidators.maxLength
import me.mtte.code.emat.validation.StandardValidators.nonNullString
import me.mtte.code.emat.validation.StandardValidators.notEmpty
import me.mtte.code.emat.validation.and
import spark.Request
import spark.Route
import spark.RouteGroup
import spark.Spark.*

object InventoryListController : RouteGroup {

    private val nameValidation = nonNullString().and(maxLength(255)).and(notEmpty());
    private val typeValidation = nameValidation;

    override fun addRoutes() {
        // Get all inventory lists
        get("", Route { _, _ -> getAllLists() }, json())

        // Create inventory list
        post("", Route(this::createList), json())

        // Get single inventory list
        get("/:id", Route(this::getList), json())

        // Update inventory list
        put("/:id", Route(this::updateList), json())

        // Delete inventory list
        delete("/:id", Route(this::deleteList), json())
    }

    private fun getAllLists(): Any? = InventoryListService.getAllInventoryLists()

    private fun getList(request: Request, response: spark.Response): Any? {
        val id = request.getId() ?: return Response.returnInvalidIdError(request, response)
        val inventoryList = InventoryListService.getInventoryList(id)
        if (inventoryList.isEmpty) {
            Response.returnInvalidIdError(request, response)
        }
        return inventoryList.get()
    }

    private fun createList(request: Request, response: spark.Response): Any? {
        val name = validateParameter(request, response, "name", nameValidation)
        val type = validateParameter(request, response, "type", typeValidation)
        val inventoryId = validateIntParameter(request, response, ":inventoryId")
        val positionId = validateIntParameter(request, response, "positionId")

        if (!InventoryService.inventoryExists(inventoryId)) {
            Response.returnInvalidParameterError(response, "inventoryId", inventoryId, "This inventory is not existing ")
        }
        if (!PositionService.positionExists(positionId)) {
            Response.returnInvalidParameterError(response, "positionId", positionId, "This position is not existing")
        }

        return InventoryListService.createInventoryList(name!!, type!!, positionId, inventoryId)
    }

    private fun updateList(request: Request, response: spark.Response): Any? {
        val name = request.getParameter("name")
        val type = request.getParameter("type")
        val inventoryId = validateIntParameter(request, response, ":inventoryId")
        val positionId = request.getParameter("positionId")

        if (!InventoryService.inventoryExists(inventoryId)) {
            Response.returnInvalidParameterError(response, "inventoryId", inventoryId, "This inventory is not existing ")
        }

        if (name != null) {
            validateParameter(request, response, "name", nameValidation)
        }
        if (type != null) {
            validateParameter(request, response, "type", typeValidation)
        }
        var positionIdInt: Int? = null
        if (positionId != null) {
            positionIdInt = validateIntParameter(request, response, "positionId")
            if (!PositionService.positionExists(positionIdInt)) {
                Response.returnInvalidParameterError(response, "positionId", positionId, "This position is not existing")
            }
        }

        val inventoryList = InventoryListService.updateInventoryList(request.getId(), name, type, positionIdInt)
        if (inventoryList.isEmpty) {
            Response.returnInvalidIdError(request, response)
        }
        return inventoryList.get()
    }

    private fun deleteList(request: Request, response: spark.Response): Any? {
        val id = request.getId() ?: return Response.returnInvalidIdError(request, response)

        if (!InventoryListService.deleteInventoryList(id)) {
            response.halt(500, MessageResponse("Error while deleting inventory list with id $id"))
        }

        return SuccessResponse()
    }

}