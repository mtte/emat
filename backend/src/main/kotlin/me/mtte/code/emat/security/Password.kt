package me.mtte.code.emat.security

import at.favre.lib.crypto.bcrypt.BCrypt

/**
 * Provides functions to handle passwords.
 */
object Password {
    fun generatePasswordHash(password: CharArray): String = BCrypt.withDefaults().hashToString(14, password)

    fun verifyPassword(password: CharArray, hash: CharArray): Boolean {
        val result = BCrypt.verifyer().verify(password, hash)
        return result.verified
    }   
}
