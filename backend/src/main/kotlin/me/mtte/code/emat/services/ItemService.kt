package me.mtte.code.emat.services

import me.mtte.code.emat.database
import me.mtte.code.emat.database.schema.emat.tables.Item.ITEM
import me.mtte.code.emat.model.Item
import java.sql.Timestamp
import java.time.Instant
import java.time.LocalDateTime
import java.util.*

object ItemService {

    fun getAllItems(): Set<Item> {
        return database.get().selectFrom(ITEM).fetch().map { Item(it) }.toSet()
    }

    fun getAllItems(listId: Int): Set<Item> {
        return database.get().selectFrom(ITEM)
            .where(ITEM.INVENTORY_LIST_ID.eq(listId))
            .map { Item(it) }.toSet()
    }

    fun getItem(id: Int): Optional<Item> {
        return database.get().fetchOptional(ITEM, ITEM.ID.eq(id)).map { Item(it) }
    }

    fun createItem(name: String, description: String, amount: Int, unit: String, userId: Int, inventoryListId: Int): Item {
        val item = database.get().newRecord(ITEM)
        item.name = name
        item.description = description
        item.amount = amount
        item.amountUnit = unit
        item.createdBy = userId
        item.created = LocalDateTime.now()
        item.modifiedBy = userId
        item.modified = LocalDateTime.now()
        item.inventoryListId = inventoryListId
        item.store()
        return Item(item)
    }

    fun deleteItem(id: Int): Boolean {
        return database.get().delete(ITEM).where(ITEM.ID.eq(id)).execute() == 1
    }

    fun updateItem(id: Int?, name: String?, description: String?, amount: Int?, unit: String?, userId: Int): Optional<Item> {
        val item = database.get().fetchOne(ITEM, ITEM.ID.eq(id)) ?: return Optional.empty()
        if (name != null) {
            item.name = name
        }
        if (description != null) {
            item.description = description
        }
        if (amount != null) {
            item.amount = amount
        }
        if (unit != null) {
            item.amountUnit = unit
        }
        if (item.changed()) {
            item.modifiedBy = userId
            item.modified = LocalDateTime.now()
        }
        item.store()
        return Optional.of(Item(item))
    }

    fun itemExists(id: Int): Boolean {
        return database.get().fetchExists(database.get().selectFrom(ITEM).where(ITEM.ID.eq(id)))
    }

}