package me.mtte.code.emat.controllers

import me.mtte.code.emat.getId
import me.mtte.code.emat.getParameter
import me.mtte.code.emat.halt
import me.mtte.code.emat.json
import me.mtte.code.emat.responses.MessageResponse
import me.mtte.code.emat.responses.Response.ResponseFactory
import me.mtte.code.emat.responses.SuccessResponse
import me.mtte.code.emat.services.InventoryService
import me.mtte.code.emat.validation.InputValidation.Companion.validateParameter
import me.mtte.code.emat.validation.StandardValidators.maxLength
import me.mtte.code.emat.validation.StandardValidators.nonNullString
import me.mtte.code.emat.validation.StandardValidators.notEmpty
import me.mtte.code.emat.validation.and
import spark.Request
import spark.Response
import spark.Route
import spark.RouteGroup
import spark.Spark.*

object InventoryController : RouteGroup {

    private val inventoryNameValidation = nonNullString().and(notEmpty()).and(maxLength(255))

    override fun addRoutes() {
        // Get all inventories
        get("", Route { _, _ -> getAllInventories() }, json())

        // Create inventory
        post("", Route(this::createInventory), json())

        // Get single inventory
        get("/:id", Route(this::getInventory), json())

        // Update inventory
        put("/:id", Route(this::updateInventory), json())

        // Delete inventory
        delete("/:id", Route(this::deleteInventory), json())
    }

    private fun getAllInventories(): Any? = InventoryService.getAllInventories()

    private fun getInventory(request: Request, response: Response): Any? {
        val id = request.getId() ?: return ResponseFactory.returnInvalidIdError(request, response)
        val inventory = InventoryService.getInventory(id)
        if (inventory.isEmpty) {
            ResponseFactory.returnInvalidIdError(request, response)
        }
        return inventory.get()
    }

    private fun createInventory(request: Request, response: Response): Any? {
        val name = validateParameter(request, response, "name", inventoryNameValidation)
        val description = request.getParameter("description") ?: ""

        return InventoryService.createInventory(name!!, description)
    }

    private fun updateInventory(request: Request, response: Response): Any? {
        val name = request.getParameter("name")
        val description = request.getParameter("description")

        if (name != null) {
            validateParameter(request, response, "name", inventoryNameValidation)
        }

        val inventory = InventoryService.updateInventory(request.getId(), name, description)
        if (inventory.isEmpty) {
            ResponseFactory.returnInvalidIdError(request, response)
        }
        return inventory.get()
    }

    private fun deleteInventory(request: Request, response: Response): Any? {
        val id = request.getId() ?: return ResponseFactory.returnInvalidIdError(request, response)

        if (!InventoryService.deleteInventory(id)) {
            response.halt(500, MessageResponse("Error while deleting inventory with id $id"))
        }

        return SuccessResponse()
    }

}