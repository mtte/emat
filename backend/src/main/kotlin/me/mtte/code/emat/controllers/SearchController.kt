package me.mtte.code.emat.controllers

import me.mtte.code.emat.getParameter
import me.mtte.code.emat.responses.Response.ResponseFactory.returnInvalidParameterError
import me.mtte.code.emat.services.SearchService
import spark.Request
import spark.Response

object SearchController {

    fun search(request: Request, response: Response): Any {
        val queryString: String = request.getParameter("query")
                ?: return returnInvalidParameterError(response, "query", null, "Invalid query")

        if (queryString.isBlank()) {
            return emptyList<Nothing>()
        }

        return SearchService.search(queryString);
    }

}