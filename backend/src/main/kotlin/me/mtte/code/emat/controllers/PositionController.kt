package me.mtte.code.emat.controllers

import me.mtte.code.emat.getId
import me.mtte.code.emat.getParameter
import me.mtte.code.emat.halt
import me.mtte.code.emat.json
import me.mtte.code.emat.responses.MessageResponse
import me.mtte.code.emat.responses.Response.ResponseFactory
import me.mtte.code.emat.responses.SuccessResponse
import me.mtte.code.emat.services.PositionService
import me.mtte.code.emat.validation.InputValidation.Companion.validateParameter
import me.mtte.code.emat.validation.StandardValidators.maxLength
import me.mtte.code.emat.validation.StandardValidators.nonNullString
import me.mtte.code.emat.validation.StandardValidators.notEmpty
import me.mtte.code.emat.validation.and
import spark.Request
import spark.Response
import spark.Route
import spark.RouteGroup
import spark.Spark.*

object PositionController : RouteGroup {

    private val positionNameValidation = nonNullString().and(notEmpty()).and(maxLength(255))

    override fun addRoutes() {
        // Get all positions
        get("", Route { _, _ -> getAllPositions() }, json())

        // Create position
        post("", Route(this::createPosition), json())

        // Get single position
        get("/:id", Route(this::getPosition), json())

        // Update position
        put("/:id", Route(this::updatePosition), json())

        // Delete position
        delete("/:id", Route(this::deletePosition), json())
    }

    private fun getAllPositions(): Any? = PositionService.getAllPositions()

    private fun getPosition(request: Request, response: Response): Any? {
        val id = request.getId() ?: return ResponseFactory.returnInvalidIdError(request, response)
        val position = PositionService.getPosition(id)
        if (position.isEmpty) {
            ResponseFactory.returnInvalidIdError(request, response)
        }
        return position.get()
    }

    private fun createPosition(request: Request, response: Response): Any? {
        val name = validateParameter(request, response, "name", positionNameValidation)!!
        val description = request.getParameter("description") ?: ""

        return PositionService.createPosition(name, description)
    }

    private fun updatePosition(request: Request, response: Response): Any? {
        val name = request.getParameter("name")
        val description = request.getParameter("description")

        if (name != null) {
            validateParameter(request, response, "name", positionNameValidation)
        }

        val position = PositionService.updatePosition(request.getId(), name, description)
        if (position.isEmpty) {
            ResponseFactory.returnInvalidIdError(request, response)
        }
        return position.get()
    }

    private fun deletePosition(request: Request, response: Response): Any? {
        val id = request.getId() ?: return ResponseFactory.returnInvalidIdError(request, response)

        if (!PositionService.deletePosition(id)) {
            response.halt(500, MessageResponse("Error while deleting position with id $id"))
        }

        return SuccessResponse()
    }

}