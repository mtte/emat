package me.mtte.code.emat.model

import me.mtte.code.emat.database.schema.emat.tables.records.InventoryRecord

data class Inventory(val id: Int, val name: String, val description: String) {
    constructor(inventoryRecord: InventoryRecord) : this(inventoryRecord.id, inventoryRecord.name, inventoryRecord.description)
}