package me.mtte.code.emat.security

import me.mtte.code.emat.getLogger
import me.mtte.code.emat.getProfile
import me.mtte.code.emat.securityConfig
import org.pac4j.sparkjava.SecurityFilter
import org.slf4j.LoggerFactory
import spark.HaltException
import spark.Request
import spark.Response


class AuthFilter(vararg roles: Role) : SecurityFilter(securityConfig, "HeaderClient",
        roles.joinToString(",") { it.toString() }) {

    override fun handle(request: Request, response: Response) {
        if (request.requestMethod() == "OPTIONS") {
            LoggerFactory.getLogger(this::class.java).debug("Received OPTIONS request -> skipping auth");
            return;
        }
        try {
            super.handle(request, response)
        } catch (e: HaltException) {
            // Catch halt exception so we can react on authorization errors
            val profile = getProfile(request, response)
            val profileInfo = profile.map { it.toString() }.orElse("NO PROFILE INFO")

            getLogger(this).warn("Client request did not pass authentication filter. Route: ${request.pathInfo()}, Profile: $profileInfo")

            throw e; // Otherwise spark halt won't work
        }
    }

}
