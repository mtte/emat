package me.mtte.code.emat.model

import me.mtte.code.emat.database.InvalidDatabaseStateException
import me.mtte.code.emat.database.schema.emat.tables.records.ItemRecord
import me.mtte.code.emat.services.InventoryListService
import me.mtte.code.emat.services.UserService
import java.time.Instant
import java.time.LocalDateTime

data class Item(
        val id: Int,
        val name: String,
        val description: String,
        val amount: Int?,
        val unit: String?,
        val inventoryListId: Int,
        val created: LocalDateTime,
        val createdBy: String,
        val modified: LocalDateTime,
        val modifiedBy: String
) {
    constructor(itemRecord: ItemRecord) : this(
            itemRecord.id,
            itemRecord.name,
            itemRecord.description,
            itemRecord.amount,
            itemRecord.amountUnit,
            itemRecord.inventoryListId,
            itemRecord.created,
            UserService.getUser(itemRecord.createdBy).orElseThrow {
                InvalidDatabaseStateException("user with id ${itemRecord.createdBy} does not exist")
            }.name,
            itemRecord.modified,
            UserService.getUser(itemRecord.modifiedBy).orElseThrow {
                    InvalidDatabaseStateException("user with id ${itemRecord.modifiedBy} does not exist")
            }.name
    )
}