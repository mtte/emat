package me.mtte.code.emat.security

import org.pac4j.core.authorization.authorizer.RequireAnyRoleAuthorizer
import org.pac4j.core.config.Config
import org.pac4j.core.config.ConfigFactory
import org.pac4j.core.profile.CommonProfile
import org.pac4j.http.client.direct.HeaderClient
import org.pac4j.jwt.config.signature.SecretSignatureConfiguration
import org.pac4j.jwt.credentials.authenticator.JwtAuthenticator
import org.pac4j.sparkjava.DefaultHttpActionAdapter


/**
 * Factory to build the configuration for pac4j that is used by the [org.pac4j.sparkjava.SecurityFilter].
 */
class SecurityConfigFactory(private val jwtSalt: String) : ConfigFactory {

    override fun build(vararg parameters: Any): Config {
        val jwtAuth = JwtAuthenticator(SecretSignatureConfiguration(jwtSalt))
        val headerClient = HeaderClient("Authorization", "Bearer", jwtAuth)
        val config = Config(headerClient)
        config.httpActionAdapter = DefaultHttpActionAdapter()

        // Add all roles
        Role.values().map(Role::toString)
            .forEach { config.addAuthorizer(it, RequireAnyRoleAuthorizer<CommonProfile>(it)) }

        return config
    }

}
