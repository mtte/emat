package me.mtte.code.emat.model

import me.mtte.code.emat.database.schema.emat.tables.records.PositionRecord

data class Position(val id: Int, val name: String, val description: String) {
    constructor(positionRecord: PositionRecord) : this(positionRecord.id, positionRecord.name, positionRecord.description)
}