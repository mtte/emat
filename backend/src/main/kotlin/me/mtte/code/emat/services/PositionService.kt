package me.mtte.code.emat.services

import me.mtte.code.emat.database
import me.mtte.code.emat.database.schema.emat.tables.Position.POSITION
import me.mtte.code.emat.model.Position
import java.util.*

object PositionService {

    fun getAllPositions(): Set<Position> {
        return database.get().selectFrom(POSITION).fetch().map { Position(it) }.toSet()
    }

    fun getPosition(id: Int): Optional<Position> {
        return database.get().fetchOptional(POSITION, POSITION.ID.eq(id)).map { Position(it) }
    }

    fun createPosition(name: String, description: String): Position {
        val position = database.get().newRecord(POSITION)
        position.name = name;
        position.description = description
        position.store()
        return Position(position)
    }

    fun deletePosition(id: Int): Boolean {
        return database.get().delete(POSITION).where(POSITION.ID.eq(id)).execute() == 1
    }

    fun updatePosition(id: Int?, name: String?, description: String?): Optional<Position> {
        val position = database.get().fetchOne(POSITION, POSITION.ID.eq(id)) ?: return Optional.empty()
        if (name != null) {
            position.name = name
        }
        if (description != null) {
            position.description = description
        }
        position.store()
        return Optional.of(Position(position))
    }

    fun positionExists(id: Int): Boolean {
        return database.get().fetchExists(database.get().selectFrom(POSITION).where(POSITION.ID.eq(id)))
    }

}