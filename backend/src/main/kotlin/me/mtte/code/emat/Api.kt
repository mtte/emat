package me.mtte.code.emat

import me.mtte.code.emat.controllers.*
import me.mtte.code.emat.security.AuthFilter
import me.mtte.code.emat.security.Role
import spark.Route
import spark.RouteGroup
import spark.Spark.*

/**
 * Defines all routes for the API.
 */
class Api : RouteGroup {

    companion object {
        private val loggedInUser = AuthFilter()
        private val adminFilter = AuthFilter(Role.ADMIN)
        private val superAdminFilter = AuthFilter(Role.SUPER_USER)
    }

    override fun addRoutes() {
        // Login
        post("/login", Route(LoginController::handleLogin), json())
        before("/authcheck", loggedInUser)
        get("/authcheck", Route { _, _ -> AuthController.checkAuth()}, json())

        // Users
        before("/users", superAdminFilter)
        before("/users/*", superAdminFilter)
        path("/users", UserController)

        // Roles
        put("/users/:id/roles/:role", Route(AuthController::addRole), json())
        delete("/users/:id/roles/:role", Route(AuthController::deleteRole), json())

        // Positions
        before("/positions", loggedInUser)
        before("/positions/*", adminFilter)
        path("/positions", PositionController)

        // Inventory
        before("/inventories", adminFilter)
        before("/inventories/", adminFilter)
        before("/inventories/:id", adminFilter)
        path("/inventories", InventoryController)

        // Inventory List
        before("/inventories/:inventoryId/lists", loggedInUser)
        before("/inventories/:inventoryId/lists/", adminFilter)
        before("/inventories/:inventoryId/lists/:id", adminFilter)
        path("/inventories/:inventoryId/lists", InventoryListController)

        // Item
        before("/inventories/:inventoryId/lists/:listId/items", loggedInUser)
        before("/inventories/:inventoryId/lists/:listId/items/", loggedInUser)
        before("/inventories/:inventoryId/lists/:listId/items/:id", loggedInUser)
        path("/inventories/:inventoryId/lists/:listId/items", ItemController)

        // Search
        before("/search", loggedInUser)
        get("/search", Route(SearchController::search), json())
    }

}
