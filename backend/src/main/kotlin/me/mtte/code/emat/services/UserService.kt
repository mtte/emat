package me.mtte.code.emat.services

import me.mtte.code.emat.database
import me.mtte.code.emat.database.schema.emat.tables.User.USER
import me.mtte.code.emat.model.User
import me.mtte.code.emat.security.Role
import org.jooq.impl.DSL
import java.util.*

object UserService {

    fun getUser(id: Int?): Optional<User> {
        val userRecord = database.get().fetchOptional(USER, USER.ID.eq(id))
        val user = userRecord.map { User(it) }
        return user.apply { ifPresent { it.roles = AuthService.getRoles(it.id) } }
    }

    fun getByUsername(username: String): Optional<User> {
        val userRecord = database.get().fetchOptional(USER, USER.NAME.eq(username))
        val user = userRecord.map { User(it) }
        user.ifPresent { it.roles = AuthService.getRoles(it.id) }
        return user
    }

    fun getUserWithPasswordByName(username: String?): Optional<User> {
        val userRecord = database.get().fetchOptional(USER, USER.NAME.eq(username))
        val user = userRecord.map { User(it) }
        user.ifPresent { it.password = userRecord.get().password }
        user.ifPresent { it.roles = AuthService.getRoles(userRecord.get().id) }
        return user
    }

    fun getAllUsers(): Set<User> {
        val result = database.get().selectFrom(USER).fetch()
        val users = result.map { User(it) }.toSet()
        users.forEach { it.roles = AuthService.getRoles(it.id) }
        return users
    }

    fun createUser(username: String, passwordHash: String, roles: Set<Role>): User {
        val userRecord = database.get().newRecord(USER)
        userRecord.name = username
        userRecord.password = passwordHash
        userRecord.store()

        val user = User(userRecord)

        roles.forEach { AuthService.addAuthorization(userRecord.id, it) }
        user.roles = roles

        return user
    }

    fun deleteUser(id: Int): Boolean {
        return database.get().transactionResult { config ->
            val transaction = DSL.using(config)

            AuthService.deleteAllAuthorizations(transaction, id)

            transaction.delete(USER)
                .where(USER.ID.eq(id))
                .execute() == 1
        }
    }

    fun updateUser(id: Int?, username: String?, passwordHash: String?): User? {
        val user = database.get().fetchOne(USER, USER.ID.eq(id)) ?: return null
        if (username != null) {
            user.name = username
        }
        if (passwordHash != null) {
            user.password = passwordHash
        }
        user.store()
        return User(user)
    }

    fun isUsernameUnique(username: String?): Boolean
            = database.get().fetchCount(USER, USER.NAME.eq(username)) == 0

}
