package me.mtte.code.emat.responses

import me.mtte.code.emat.getId
import me.mtte.code.emat.halt
import me.mtte.code.emat.security.Role
import me.mtte.code.emat.toJson
import spark.Request

/**
 * This is the payload of a [spark.Response].
 */
interface Response {

    fun toJson(): String {
        return toJson(this)
    }

    companion object ResponseFactory {

        fun returnInvalidIdError(request: Request, response: spark.Response) {
            response.halt(400, MessageResponse("The supplied id '${request.getId()}' is invalid"))
        }

        fun returnInvalidParameterError(response: spark.Response, parameter: String, value: Any?, message: String) {
            response.halt(400, MessageResponse("Invalid Parameter: '$parameter' with value '$value'. $message."))
        }

        fun invalidRequestError(response: spark.Response, message: String) {
            response.halt(400, MessageResponse("Invalid Request: $message"))
        }

    }
}

class SuccessResponse : Response {
    val message = "success"
}

data class MessageResponse(val message: String) : Response

data class LoginResponse(
    val token: String,
    val username: String,
    val roles: Set<Role>
) : Response