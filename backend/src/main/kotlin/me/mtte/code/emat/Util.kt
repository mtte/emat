package me.mtte.code.emat

import com.google.gson.Gson
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spark.ResponseTransformer

/**
 * Utility to get the logger.
 * @param any The class of any will be used to get the logger. Normally called with `this`. If any is a String it will
 *            be used as the name.
 * @return The logger for [any]
 */
fun getLogger(any: Any): Logger {
    return when (any) {
        is String -> LoggerFactory.getLogger(any)
        else -> LoggerFactory.getLogger(any::class.java)
    }
}

/**
 * Converts the given object to it's JSON representation as [String].
 * @param toConvert The object to convert
 * @return The JSON representation of the object
 */
fun toJson(toConvert: Any): String = Gson().toJson(toConvert)

/**
 * Returns a [ResponseTransformer] that converts the response object into a json string.
 * @return Converts the response to json.
 */
fun json() = ResponseTransformer(::toJson)

/**
 * Convert a [String] to a [Int]
 * @return The string value as integer, will be `null` if the string is not a valid number
 */
fun convertToInt(value: String?): Int? {
    return try {
        value?.toInt()
    } catch (e: NumberFormatException) {
        null
    }
}

/**
 * Check if an enum value with the given name exists.
 * @param name The name of an enum value
 * @param T The enum to check against
 */
inline fun <reified T : Enum<T>> isValidEnumValue(name: String?): Boolean =
    enumValues<T>().any { e -> e.name == name }
