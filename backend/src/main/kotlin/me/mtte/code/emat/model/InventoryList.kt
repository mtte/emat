package me.mtte.code.emat.model

import me.mtte.code.emat.database.InvalidDatabaseStateException
import me.mtte.code.emat.database.schema.emat.tables.records.InventoryListRecord
import me.mtte.code.emat.services.InventoryService
import me.mtte.code.emat.services.PositionService

data class InventoryList(
        val id: Int,
        val name: String,
        val inventory: Inventory,
        val position: Position,
        val type: String
) {
    constructor(inventoryListRecord: InventoryListRecord) : this(
            inventoryListRecord.id,
            inventoryListRecord.name,
            InventoryService.getInventory(inventoryListRecord.inventoryId).orElseThrow {
                InvalidDatabaseStateException("Inventory list with id ${inventoryListRecord.inventoryId} does not exist")
            },
            PositionService.getPosition(inventoryListRecord.positionId).orElseThrow {
                InvalidDatabaseStateException("Position with id ${inventoryListRecord.positionId} does not exist")
            },
            inventoryListRecord.type
    )
}