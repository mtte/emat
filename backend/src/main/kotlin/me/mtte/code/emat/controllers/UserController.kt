package me.mtte.code.emat.controllers

import me.mtte.code.emat.*
import me.mtte.code.emat.model.User
import me.mtte.code.emat.responses.MessageResponse
import me.mtte.code.emat.responses.Response.ResponseFactory.returnInvalidIdError
import me.mtte.code.emat.responses.Response.ResponseFactory.returnInvalidParameterError
import me.mtte.code.emat.responses.SuccessResponse
import me.mtte.code.emat.security.Password
import me.mtte.code.emat.security.Role
import me.mtte.code.emat.services.AuthService
import me.mtte.code.emat.services.UserService
import me.mtte.code.emat.validation.*
import me.mtte.code.emat.validation.InputValidation.Companion.validateParameter
import me.mtte.code.emat.validation.StandardValidators.maxLength
import me.mtte.code.emat.validation.StandardValidators.minLength
import me.mtte.code.emat.validation.StandardValidators.nonNullString
import spark.Request
import spark.Response
import spark.Route
import spark.RouteGroup
import spark.Spark.*
import java.util.*
import java.util.function.Predicate

object UserController : RouteGroup {

    private val usernameValidation: Validator<String?>
        get() {
            val isUsernameUnique = BooleanValidator(Predicate(UserService::isUsernameUnique), "Username already exists")
            return nonNullString().and(StandardValidators.notEmpty()).and(maxLength(50)).and(isUsernameUnique)
        }

    private val passwordValidation: Validator<String?> = nonNullString()
        .and(minLength(8))
        .and(maxLength(50))
        .and(PasswordDiversityValidator())

    private val roleValidation: Validator<String?>
        get() {
            val roleExists = BooleanValidator<String?>(Predicate { isValidEnumValue<Role>(it) }, "Role does not exist")
            return nonNullString().and(StandardValidators.notEmpty()).and(roleExists)
        }

    override fun addRoutes() {
        // Get all users
        get("", Route { _, _ -> getAllUsers() }, json())

        // Create user
        post("", Route(this::createUser), json())

        // Get single user
        get("/:id", Route(this::getUser), json())

        // Update user
        put("/:id", Route(this::updateUser), json())

        // Delete user
        delete("/:id", Route(this::deleteUser), json())
    }

    private fun getAllUsers() = UserService.getAllUsers()

    private fun getUser(request: Request, response: Response): Any? {
        val id = request.getId()
        if (id != null) {
            val user: Optional<User> = UserService.getUser(id)
            if (user.isPresent) {
                return user.get()
            }
        }
        return returnInvalidIdError(request, response)
    }

    private fun createUser(request: Request, response: Response): Any? {
        val username = validateParameter(request, response, "username", usernameValidation)!!
        val password = validateParameter(request, response, "password", "*****", passwordValidation)!!

        val roles = getRoles(request, response)

        val hash = Password.generatePasswordHash(password.toCharArray())
        return UserService.createUser(username, hash, roles)
    }

    private fun getRoles(request: Request, response: Response): Set<Role> {
        val roles = mutableSetOf(Role.USER)

        val roleParam = request.getArrayParameter<String>("roles")
        if (roleParam != null && roleParam.isNotEmpty()) {
            for (roleStr in roleParam) {
                val validation = Validation(roleStr, roleValidation)
                if (validation.failed()) {
                    returnInvalidParameterError(response, "roles", roleStr, validation.getError())
                }
                roles.add(Role.valueOf(roleStr))
            }
        }

        return roles
    }

    private fun updateUser(request: Request, response: Response): Any? {
        val password = request.getParameter("password")
        var passwordHash: String? = null
        if (password != null) {
            validateParameter(request, response, "password", "*****", passwordValidation)
            passwordHash = Password.generatePasswordHash(password.toCharArray())
        }

        val username = request.getParameter("username")
        val currentUser = UserService.getUser(request.getId())
        if (username != null && !(currentUser.isPresent && username == currentUser.get().name)) {
            validateParameter(request, response, "username", usernameValidation)
        }

        return UserService.updateUser(request.getId(), username, passwordHash)
            ?: return returnInvalidIdError(request, response)
    }

    private fun deleteUser(request: Request, response: Response): Any? {
        val id = request.getId()
            ?: return returnInvalidIdError(request, response)

        if (!UserService.deleteUser(id)) {
            response.halt(500, MessageResponse("Error while deleting user with id $id"))
        }

        return SuccessResponse()
    }

}
