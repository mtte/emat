-- SCRIPT TO CREATE THE DATABASE

-- SCHEMA emat
CREATE SCHEMA IF NOT EXISTS emat;

-- TABLE user
CREATE TABLE IF NOT EXISTS emat.user
(
    id SERIAL NOT NULL,
    name VARCHAR(50)  NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

-- TABLE authorization
CREATE TABLE IF NOT EXISTS emat.authorization
(
    user INT NOT NULL,
    role VARCHAR(25) NOT NULL,
    PRIMARY KEY (user, role),
    FOREIGN KEY (user) REFERENCES emat.user (id)
);

-- TABLE position
CREATE TABLE IF NOT EXISTS emat.position
(
    id SERIAL NOT NULL,
    name VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    PRIMARY KEY (id)
);

-- TABLE inventory
CREATE TABLE IF NOT EXISTS emat.inventory
(
    id SERIAL NOT NULL,
    name VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    PRIMARY KEY (id)
);

-- TABLE inventory_list
CREATE TABLE IF NOT EXISTS emat.inventory_list
(
    id SERIAL NOT NULL,
    name VARCHAR(255) NOT NULL,
    inventory_id INT NOT NULL,
    position_id INT NOT NULL,
    type VARCHAR(255) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (inventory_id) REFERENCES emat.inventory (id),
    FOREIGN KEY (position_id) REFERENCES emat.position (id)
);

-- TABLE item
CREATE TABLE IF NOT EXISTS emat.item
(
    id SERIAL NOT NULL,
    name VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    amount INT,
    amount_unit VARCHAR(25),
    inventory_list_id INT NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    created_by INT NOT NULL,
    modified TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified_by INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (inventory_list_id) REFERENCES emat.inventory_list (id),
    FOREIGN KEY (created_by) REFERENCES emat.user (id),
    FOREIGN KEY (modified_by) REFERENCES emat.user (id)
);
