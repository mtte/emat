-- SCRIPT TO FILL INITIAL DATA INTO THE DB

-- CREATE USERS IF NONE EXIST
INSERT INTO emat.user SELECT * FROM (
    SELECT 1, 'su', '$2a$14$sQ8zPYv.5zfzE4G6m.MlYu444jp5ULZp8sG1iQIOGwAExu7H3wvne' UNION
    SELECT 2, 'admin', '$2a$14$sQ8zPYv.5zfzE4G6m.MlYu444jp5ULZp8sG1iQIOGwAExu7H3wvne' UNION
    SELECT 3, 'test', '$2a$14$sQ8zPYv.5zfzE4G6m.MlYu444jp5ULZp8sG1iQIOGwAExu7H3wvne'
) x WHERE NOT EXISTS (SELECT * FROM emat.user);

-- CREATE AUTHORIZATIONS IF NONE EXIST
INSERT INTO emat.authorization SELECT * FROM (
    SELECT 1, 'SUPER_USER' UNION
    SELECT 1, 'ADMIN' UNION
    SELECT 1, 'USER' UNION
    SELECT 2, 'ADMIN' UNION
    SELECT 2, 'USER' UNION
    SELECT 3, 'USER'
) x WHERE NOT EXISTS (SELECT * FROM emat.authorization);

-- CREATE INVENTORY IF NONE EXIST
INSERT INTO emat.inventory SELECT * FROM (
    SELECT 1, 'Sidus', 'Sidus Material'
) x WHERE NOT EXISTS (SELECT * FROM emat.inventory);

-- CREATE DEFAULT POSITIONS IF NONE EXIST
INSERT INTO emat.position SELECT * FROM (
    SELECT 1, '1', 'Position 1' UNION
    SELECT 2, '2', 'Position 2'
) x WHERE NOT EXISTS (SELECT * FROM emat.position);