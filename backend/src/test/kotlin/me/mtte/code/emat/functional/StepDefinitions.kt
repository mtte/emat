package me.mtte.code.emat.functional

import io.cucumber.java8.En
import io.cucumber.java8.PendingException
import org.slf4j.LoggerFactory

class StepDefinitions : En {

    init {
        Given("There are {int} users existing") { int1: Int? ->
            LoggerFactory.getLogger("CUCUMBER").info("Given {}", int1)
        }

        When("I call {word} using {word}") { path: String?, method: String? ->
            LoggerFactory.getLogger("CUCUMBER").info("When {} {}", path, method)
        }

        Then("I should receive {int} users") { int1: Int? ->
            LoggerFactory.getLogger("CUCUMBER").info("Then {}", int1)
        }
    }

}