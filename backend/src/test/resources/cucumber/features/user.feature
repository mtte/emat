Feature: User
  Test the user endpoints

  Scenario: Get users
    Given There are 3 users existing
    When I call /users using GET
    Then I should receive 3 users