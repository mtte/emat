# Api Specification

## Endpoints

### Login

```http
POST /api/login
```

Required parameters: **username, password**



------



### Users

#### List all Users

```http
GET /api/users
```

#### Get User

```http
GET /api/users/{id}
```

#### Create User

```http
POST /api/users
```

Required parameters: **username, password**
Optional parameters: **roles**

#### Update User

```http
PUT /api/users/{id}
```

Optional parameters: **username, password**

#### Delete User

```http
DELETE /api/users/{id}
```



------



### Authorization

#### Add Role to User

```http
PUT /api/users/{id}/roles/{role}
```

#### Remove Roles from User

```http
DELETE /api/users/{id}/roles/{role}
```



------



### Position

#### List all Positions

```http
GET /api/positions
```

#### Get Position

```http
GET /api/positions/{id}
```

#### Create Position

```http
POST /api/positions
```

Required parameters: **name, description**

#### Update Position

```http
PUT /api/position/{id}
```

Optional parameters: **name, description**

#### Delete Position

```http
DELETE /api/position/{id}
```



------



### Inventory

#### List all Inventories

```http
GET /api/inventories
```

#### Get Inventory

```http
GET /api/inventories/{id}
```

#### Create Inventory

```http
POST /api/inventories
```

Required parameters: **name, description**

#### Update Inventory

```http
PUT /api/inventories/{id}
```

Optional parameters: **name, description**

#### Delete Inventory

```http
DELETE /api/inventories/{id}
```



------



### Inventory Lists

#### List all Inventory Lists

```http
GET /api/inventories/{id}/lists
```

#### Get Inventory List

```http
GET /api/inventories/{id}/lists/{id}
```

#### Create Inventory List

```http
POST /api/inventories/{id}/lists
```

Required parameters: **name, type, positionId**

#### Update Inventory List

```http
PUT /api/inventories/{id}/lists/{id}
```

Optional parameters: **name, type, positionId**

#### Delete Inventory List

```http
DELETE /api/inventories/{id}/lists/{id}
```



------



### Item

#### List all Items

```http
GET /api/inventories/{id}/lists/{id}/items
```

#### Get Item

```http
GET /api/inventories/{id}/lists/{id}/items/{id}
```

#### Create Item

```http
POST /api/inventories/{id}/lists/{id}/items
```

Required parameters: **name, description, amount, unit**

#### Update Item

```http
PUT /api/inventories/{id}/lists/{id}/items/{id}
```

Optional parameters: **name, description, amount, unit**

#### Delete Item

```http
DELETE /api/inventories/{id}/lists/{id}/items/{id}
```

